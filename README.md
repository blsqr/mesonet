# Mesonet

The mesonet model (from μέσο, _gr._: means, way, resource) investigates the interaction of abstracted populations with the resources in their environment.

More specifically, this model implements an abstracted consumer-resource network where populations are utilising and competing for the resources in their environment.
With the ability of populations to split (once a certain size is reached) and the offspring-populations to receive mutations, an evolutionary element is added...

For more information on the workings of the model, see the [Wiki](https://ts-gitlab.iup.uni-heidelberg.de/yunus/mesonet/wikis/home).

The model is implemented as the `mesonet` package which is using [deeevoLab](https://ts-gitlab.iup.uni-heidelberg.de/yunus/deeevoLab) as a simulation framework. This way, the basic tools for configuring, performing, and evaluating complex simulations are already supplied and `mesonet` can focus on the implementation of the model.

This whole project is a result of my Master thesis, mainly worked on from March to December 2017. It was consequently cleaned up and will remain in development.

For any kind of questions or suggestions, feel free to reach out, be it via [e-mail](mailto:yunus.sevinchan@iup.uni-heidelberg.de) or by [writing an issue](https://ts-gitlab.iup.uni-heidelberg.de/yunus/mesonet/issues) here in the GitLab.

Cheers,  
— Yunus


# Installation
### Docker
Instead of installing mesonet on your machine, you can use [docker](https://www.docker.com) for running a container where everything is already setup as needed.
This way, you are mostly independent on the host system you are using.

After you have [downloaded](https://www.docker.com/get-docker) and installed docker, open a terminal and move to the directory where you want to work on deeevoLab and mesonet in:
```
cd /where/i/want/to/work/on/mesonet
```

Now, create a folder that will be used to exchange data between the docker container and your host system. There, the simulation output will be stored or you can transfer configuration files back and forth.
```
mkdir shared
```

After having done so, you can start the container with the following command:
```
docker run -it --name mesonet -w /home/user -u user --mount type=bind,source="$(pwd)"/shared,target=/home/user blsqr/mesonet:latest /bin/bash
```

This command will download the docker image and start up a container; this may take a little while (during which you will get no feedback on the progress). Once ready, you will see that your prompt changed to something like 
```
[root@017fa5a05e6a user]# 
```
You are now inside the docker container.

### Inside the docker container
Available commands are, among others, `python` and `ipython`. Within python, you are able to `import deeevolab as dvl` and `import mesonet`.

Importantly, you can use the deeevoLab CLI to run simulations. With the test configuration already available in the image, you can call:
```
dvl-run mesonet test_cfg.yml
```
Use the `shared` directory to supply other configuration files. See the [Getting Started](#getting-started) section on how to proceed.

You can even start up Jupyter notebooks; for a how-to, see the documentation of the [graph-tool docker image](https://git.skewed.de/count0/graph-tool/wikis/installation-instructions#installing-using-docker), on which this image is based. All steps described there are also possible to perform with this image.

### Leaving the container and restarting
To leave a container, simple execute `exit`.

To get back in, you can use
```
docker container start -a mesonet
```
to start it and then attach to it.

*Note:* The container itself might not store data persistently. Make sure to put everything you desire to keep into the `/home/user` directory inside the container, which is coupled to the local `shared` directory.

If something went completely wrong, you can always delete the container (using `docker container stop` and `docker container rm`) and call the `docker run` command again to start from zero. The data stored in `shared` will persist.

### Manual installation
#### 0 — _Optional:_ Create a virtual environment
For a cleaner working environment, it is good practice to install all python dependencies into a virtual environment.

To create one based on Python 3, run the following commands:

```
$ cd ~
$ mkdir .virtualenvs
$ cd .virtualenvs
$ python3 -m venv mesonet --system-site-packages
```

This will create a virtual environment `~/.virtualenvs/mesonet`.
You can enter and exit it with the following commands, respectively:

```
$ source ~/.virtualenvs/mesonet/bin/activate
...
(mesonet) $ deactivate
```

In order for the following python dependencies being installed into that environment, make sure that you have entered the virtual envionment, indicated by the prefixed `(mesonet)` in your shell.


#### 1 — Install deeevoLab and its dependencies
With `mesonet` being based on the deeevoLab framework, the main part of the installation is also the installation of deeevoLab.

For that, follow the steps described in the [deeevoLab README](https://ts-gitlab.iup.uni-heidelberg.de/yunus/deeevoLab#manual-installation).

#### 2 — Install `mesonet`
##### Installation only
If you will not work on the code directly but only on the configuration and execution of simulations, this installation should suffice.

Run the following command to install the latest release of `mesonet`:

```
$ pip install \
	git+https://ts-gitlab.iup.uni-heidelberg.de/yunus/mesonet@release/latest \
	--trusted-host ts-gitlab.iup.uni-heidelberg.de
```

To install the current master branch, remove the branch specification (the part after the `@`).

##### Clone and install
If you desire to work directly on the code base of `mesonet` and contribute to the repository, cloning and installing locally is recommended.

```
$ git clone https://ts-gitlab.iup.uni-heidelberg.de/yunus/mesonet.git
```
If you get an `SSLError`, prefix `GIT_SSL_NO_VERIFY=true ` to the command, which disables the certificate verification for the duration of the command.

After that, `mesonet` can be installed locally via:

```
$ pip install -e mesonet/.[test_deps]
```

The `-e` installs in editable mode, meaning that symlinks are created rather than the source files being copied to another location; this makes development easier.
The `[test_deps]` additionally installs the dependencies needed for testing.


# Usage
### Getting started
To execute a simulation of the mesonet model, the deeevoLab command line interface (CLI) can be used:

```
$ dvl-run mesonet path/to/cfg.yml
```

The config file given needs to adhere to the correct structure. Configurations that are tested with the current version of the model are supplied in the [`tests/cfg` directory](https://ts-gitlab.iup.uni-heidelberg.de/yunus/mesonet/tree/master/tests/cfg). Copy these files to a location where you can store different versions of these that you can call to run different scenarios of the model. Then, at that location, change the files to your needs, and pass them to the CLI.

For the full CLI, add the `--help` flag and have a look at the [deeevoLab README](https://ts-gitlab.iup.uni-heidelberg.de/yunus/deeevoLab#command-line-interface-cli).


### Testing
If you change parts of the code, it is important to be able to validate that the changes did not break any other part of the model.  
To that end, `mesonet` employs a testing framework, `pytest`, which executes these tests and determines, e.g., how much code is covered by these tests.

Currently, the tests do not cover all the code and there are little assertion statements which are important to, well, assert that a piece of code behaves in a certain way. It is part of active development to expand these tests.

To run the test suite, make sure to have installed `mesonet` together with the test dependencies. Then, call the following command:

```
$ python -m pytest -v tests/ --cov=mesonet --cov-report=term-missing
```

This will perform the tests and print a so-called coverage report, indicating which parts of the code were tested and which were not.
