"""mesonet is an evolutionary interaction network of populations with the resources in their environment."""

__version__ = "0.4"

# Set up the deeevolab logger and use those tools to set up a mesonet logger
import deeevolab as dvl

dvl.setup_root("mesonet")

# Setup logging for this file (inherits from root logger)
log	= dvl.setup_logger(__name__)
log.debug("Loaded module and logger.")

log.highlight("Initialising mesonet project ...")


# Now, supply frequently used classes
from .mesonet import Mesonet
from .resourcenet import ResourceNetwork

# ...and a special name for the CLI of deeevoLab to get the default universe
Universe = Mesonet
base_uni_cfg = {}
