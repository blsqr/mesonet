"""This module implements the Genealogy class that keeps track of how the genealogy of populations develops."""

import deeevolab as dvl
import deeevolab.network

# Setup logging for this file (inherits from root logger)
log	= dvl.setup_logger(__name__, level=dvl.get_log_level())
cfg = dvl.get_config(__name__)
log.debug("Loaded module and logger.")

# Local constants
DEBUG = dvl.get_debug_flag()

# -----------------------------------------------------------------------------

class Genealogy(dvl.network.Network):
	"""The Genealogy keeps track of the Population's ancestral tree and
	properties that are constant or mostly constant during their lifetime.

	Note: The genealogy should only hold properties that are not dynamically
	changing during the lifetime of the populations. For example, the
	innovations and number of roots are changing a lot during the lifetime,
	and should thus be stored in the ResourceNetwork.
	"""

	# Use custom plotting styles
	PLOT_STYLES = cfg.plot_styles

	# The following attributes are updated when a new vertex is added to this network.
	INIT_FROM_RNET = [
			'birth', 'alive', 'demise', 'birth_rate',
			'use_reserves', 'p_innov', 'split_size',
			('_theta_K', 'theta_K'), # (source attr name, target attr)
			('num_roots', 'num_roots_init'),
			('num_innovs', 'num_innovs_init'),
	]


	def __init__(self, *_, **nw_kwargs):
		"""Sets up the ancestral tree's property maps."""

		log.note("Initialising %s ...", self.__class__.__name__)

		# Initialise from the Network class
		super().__init__(directed=True, **nw_kwargs)

		# Setup vertex properties
		self.vp['pop_index']		= self.new_vp('int')
		self.vp['generation'] 		= self.new_vp('int')

		self.vp['birth']	 		= self.new_vp('int')
		self.vp['alive'] 			= self.new_vp('bool')
		self.vp['demise'] 			= self.new_vp('int')

		self.vp['theta_K']			= self.new_vp('float')
		self.vp['birth_rate']		= self.new_vp('float')
		self.vp['use_reserves']		= self.new_vp('float')

		self.vp['p_innov']			= self.new_vp('float')
		self.vp['num_roots_init']	= self.new_vp('int')
		self.vp['num_innovs_init']	= self.new_vp('int')

		self.vp['split_size']		= self.new_vp('int')

		# Setup graph properties
		self.gp['ratio_alive']		= self.new_gp('float')
		self.gp['max_gen']			= self.new_gp('int')
		self.gp['mean_gen']			= self.new_gp('float')
		self.gp['oldest_alive']		= self.new_gp('int')
		self.gp['min_gen_alive']	= self.new_gp('int')
		self.gp['max_gen_alive']	= self.new_gp('int')
		self.gp['mean_gen_alive']	= self.new_gp('float')

		log.debug("Finished initialising.")


	# Plotting extensions .....................................................

	def _vp_color_from_age(self, gv, props, **kwargs):
		"""Returns a color depending on the age of the vertex. If a population is still alive, it is computed towards the current step count."""

		# Create the vertex property map
		vp = self.new_vp('int')

		# Fill it with values
		for vtx in self.vertices():
			if self.vp['alive'][vtx]:
				vp[vtx]	= self.location.steps - self.vp['birth'][vtx]

			else:
				vp[vtx]	= self.vp['demise'][vtx] - self.vp['birth'][vtx]

		# Do the rest in the helper method ...
		return self._color_from_vp(gv, props, vp, **kwargs)

	def _vp_color_from_split_size(self, gv, props, *_, val_range: list=None, center_first_val: bool=False, **kwargs):
		"""Returns a color depending on the split_size of the Population.

		If the center_first_val option is given, the first value is used for defining val_range."""

		if center_first_val:
			# Get the first value
			# FIXME does this make sense / work?!
			log.debugv("Getting central value for split_size")
			val_range = self.vp['split_size'][0]

		# Nothing to do here, just call the parent method
		return self._vp_color_from_property(gv, props,
		                                    vp_name='split_size',
		                                    val_range=val_range,
		                                    **kwargs)

	def _vp_color_from_num_new_roots(self, gv, props, **kwargs):
		"""Returns a color depending on the number of new roots in this vertex compared with the initial value."""

		# Create the vertex property map
		vp = self.new_vp('int')

		# Fill it with values
		for vtx in self.vertices():
			rnet_vobj = self.link.vobjs[vtx]
			vp[vtx] = rnet_vobj.num_roots - self.vp['num_roots_init'][vtx]

		# Do the rest in the helper method ...
		return self._color_from_vp(gv, props, vp, **kwargs)


	def _vp_color_from_num_new_innovs(self, gv, props, **kwargs):
		"""Returns a color depending on the number of new innovations in this vertex compared with the initial value."""

		# Create the vertex property map
		vp = self.new_vp('int')

		# Fill it with values
		for vtx in self.vertices():
			rnet_vobj = self.link.vobjs[vtx]
			vp[vtx] = rnet_vobj.num_innovs - self.vp['num_innovs_init'][vtx]

		# Do the rest in the helper method ...
		return self._color_from_vp(gv, props, vp, **kwargs)
