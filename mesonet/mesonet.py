"""This is the Universe class for the mesonet project, which investigates evolution in a resource network."""

import copy

import numpy as np
import scipy.stats

import deeevolab as dvl
import deeevolab.universe

from mesonet.resourcenet import ResourceNetwork

# Initialise loggers and config file
log = dvl.setup_logger(__name__, level=dvl.get_log_level(is_universe=True))
cfg = dvl.get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Numpy error level settings
np.seterr(divide='ignore', invalid='ignore')

# Local Constants
DEBUG = dvl.get_debug_flag()

# -----------------------------------------------------------------------------

class Mesonet(dvl.universe.Universe):
	"""The Universe is where everything takes place. It is a child of the ParentUniverse class and provides an interface for management of the simulation, plotting of data and saving of data.

	# TODO write what the Mesonet universe does
	"""

	def __init__(self, *args, network_setup: dict, rnet_kwargs: dict, mesonet_monitors: dict, **kwargs):
		"""Initialise the Mesonet universe"""

		# Initialise the parent universe
		super().__init__(*args, **kwargs)

		# Set the possible flags
		self.flags.update_genealogy = False

		# Initialise and setup ResourceNetwork
		self.network = ResourceNetwork(location=self, **rnet_kwargs)
		self._setup_network(**network_setup)

		# Initialise monitors here, if needed
		self.setup_monitors(**mesonet_monitors)

		# Save the initialisation parameters as metadata
		# args and kwargs are saved in ParentUniverse
		log.debug("Adding Universe metadata to init_kwargs ...")
		self.metadata.update_item('init_kwargs',
		                          dict(network_setup=network_setup))

		# Use the method implemented in ParentUniverse to finish initialisation
		self._init_finished()

		return

	def _setup_network(self, *_, primary: dict, secondary: dict, populations: dict, initial_propagation: bool=True, source_and_sink: dict=None):

		# Prepare dict to gather the resolved parameters
		nw_setup = dict(primary=[], secondary=[], populations=[])

		# Get parameters for resources and populations
		num_primary	= primary.pop('num')
		num_secondary = secondary.pop('num')
		num_populations	= populations.pop('num')

		log.note("Setting up ResourceNetwork with %d primary resources, "
		         "%d secondary resources, and %d populations ...",
		         num_primary, num_secondary, num_populations)

		# Extract the explicit update lists and configs
		primary_update = primary.pop('explicit_update', None)
		secondary_update = secondary.pop('explicit_update', None)
		populations_update = populations.pop('explicit_update', None)

		primary_update_cfg = primary.pop('explicit_update_cfg', {})
		secondary_update_cfg = secondary.pop('explicit_update_cfg', {})
		populations_update_cfg = populations.pop('explicit_update_cfg', {})

		# Prepared the parameters now, start with adding nodes . . . . . . . .
		# First add source and sink
		self.network.add_source_and_sink(**(source_and_sink
		                                    if source_and_sink else {}))

		# Now add the resources and populations, resolving the generators in the configuration before passing the parameters. All parameters are saved in the nw_setup dict...
		for i in range(num_primary):
			# Prepare the parameters, then create the primary resource
			_params = self.prepare_nw_params(primary,
			                                 idx=i, len_iter=num_primary,
			                                 update_list=primary_update,
			                                 **primary_update_cfg)
			self.network.add_primary_resource(**_params)

			# Save to the dictionary
			nw_setup['primary'].append(_params)

		for i in range(num_secondary):
			# Prepare the parameters, then create the secondary resource
			_params = self.prepare_nw_params(secondary,
			                                 idx=i, len_iter=num_secondary,
			                                 update_list=secondary_update,
			                                 **secondary_update_cfg)
			self.network.add_secondary_resource(**_params)

			# Save to the dictionary
			nw_setup['secondary'].append(_params)

		for i in range(num_populations):
			# Prepare the parameters, then create the population
			_params = self.prepare_nw_params(populations,
			                                 idx=i, len_iter=num_populations,
			                                 update_list=populations_update,
			                                 **populations_update_cfg)
			self.network.add_population(**_params)

			# Save to dictionary
			nw_setup['populations'].append(_params)

		# Done creating resources and populations . . . . . . . . . . . . . . .
		log.debug("Network with %d vertices and %d edges created.",
		          self.network.num_vertices(), self.network.num_edges())

		# Add the initialisation kwargs to metadata
		self.metadata.update_item('init_kwargs',
		                          dict(network_setup_resolved=nw_setup))

		# Propagate the energy from the EnergySource once -- this reduces initialisation effects.
		if initial_propagation:
			log.note("Performing initial propagation from EnergySource ...")
			self.network.propagate_energy(vtx_list=[self.network.src_vtx])
		else:
			log.note("NOT performing initial propagation from EnergySource!")

		# Update property maps to have initial values
		self.network.calc_global_energy_measures()

		log.debug("ResourceNetwork is set up.")

		return

	# Simulation ..............................................................

	def run_simulation_step(self) -> bool:
		"""Runs one step of the network simulation. Is called by
		run_simulation(), which is implemented in the parent Universe.
		"""

		# If any actions were performed that require an overall update of the genealogy, this is done in the following. It is rather costly and should thus not be done too frequently.
		if self.flags.update_genealogy:
			self.network.manual_update_genealogy()
			self.flags.update_genealogy = False

		# Reset the energy changes (needed in propagation)
		self.network.reset_energy_changes()

		# Propagate energy from EnergySource to Resources and from Resources 
		# to Populations
		self.network.propagate_energy(select_from='not_pops')

		# Population dynamics
		self.network.population_dynamics()

		# Innovations and population splitting
		self.network.population_splitting()
		self.network.add_innovations()

		# Propagate energy from alive populations to the energy sink
		self.network.propagate_energy(select_from='alive')

		# Update some measures that are needed in monitors
		self.network.calc_global_energy_measures()
		# TODO move to an action

		# Everything ok. Return True. Stop conditions are checked after this...
		return True

	# Action methods ..........................................................
	# Changes to the network . . . . . . . . . . . . . . . . . . . . . . . . .

	def _actn_add_vertices(self, *, vtx_type: str, num: int=1, explicit_update: list=None, explicit_update_cfg: dict=None, **res_kwargs):
		"""Adds a vertex to the resource network
		
		Args:
		    vtx_type (str): The type of the vertices to add; possible values
		     	are: 'res1', 'res2', 'pop'
		    num (int, optional): Number of resources to add of this type
		    **res_kwargs: The arguments to initialise the resources with
		"""

		# Resolve the resource class
		if vtx_type == 'res1':
			add_vtx = self.network.add_primary_resource

		elif vtx_type == 'res2':
			add_vtx = self.network.add_secondary_resource
		
		elif vtx_type == 'pop':
			add_vtx = self.network.add_population

		else:
			raise ValueError("Allowed vertex types: res1, res2, pop. Was: "
			                 + str(vtx_type))
		
		log.progress("Adding %d resource(s) of type %s to resource network...",
		             num, vtx_type)

		# Dict to store the results in
		added_vertices = {}

		if not explicit_update_cfg:
			explicit_update_cfg = {}

		# Add vertices
		for i in range(num):
			# Parse parameters
			params = self.prepare_nw_params(res_kwargs,
			                                idx=i, len_iter=num,
			                                update_list=explicit_update,
			                                **explicit_update_cfg)

			# Call function that adds the vertex to the network
			vtx, res = add_vtx(**params)

			# Store parameters under vertex number in results dict
			added_vertices[int(vtx)] = params
			added_vertices[int(vtx)]['type'] = vtx_type

		return added_vertices
	_actn_add_vertices.APPEND_RES = True

	# Changes to the monitors . . . . . . . . . . . . . . . . . . . . . . . . .

	def _actn_add_monitors(self, **mon_kwargs):
		"""Passes the given arguments to the (inherited) setup_monitors method.
		"""
		self.setup_monitors(**mon_kwargs)

	# Perturbations . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def _actn_change_energy_input(self, factor: float=1., offset: float=0.):
		"""Changes the energy input to all secondary resources by the given
		factor and then adds the offset.

		This is done for each secondary resource separately. Thus, if the
		factor and/or offset are RNG instances, the values can be changed.

		Returns a dictionary of the applied factors and offsets.
		"""

		log.progress("Changing energy input of %d secondary resources ...",
		             self.network.gvs['res2'].num_vertices())

		changes = {}

		for vtx in self.network.vertices(select_from='res2'):
			log.debug("Vertex %s ...", vtx)
			e = (self.network.src_vtx, vtx)

			_factor = dvl.rng.call_if_rng(factor)
			_offset = dvl.rng.call_if_rng(offset)

			if _factor == 1. and _offset == 0.:
				log.debugv("No changes for vertex %s", vtx)
				continue

			log.debugv("Changing energy input of %s by factor %s, "
			           "offset %s ...", vtx, _factor, _offset)

			changes[int(vtx)] = dict(factor=_factor, offset=_offset,
			                         old=self.network.ep['max_flow'][e])

			self.network.ep['max_flow'][e] *= _factor
			self.network.ep['max_flow'][e] += _offset

			changes[int(vtx)]['new'] = self.network.ep['max_flow'][e]

		return changes
	_actn_change_energy_input.APPEND_RES = True

	def _actn_change_vtx_param(self, select_from: str, param_name: str, factor: float=None, offset: float=None, add_items: list=None, remove_items: list=None, set_flags: list=None, ignore_missing_item: bool=True):
		"""Changes the parameter of a vertex using the given parameters.

		This is done for each vertex separately. Thus, if the factor and/or offset are RNG instances, the values can differ.

		Args:
			select_from:	name of the GraphView to select the vertices
			param_name: 	the name of the parameter to change. If this is a list, it is interpreted as a list of keys to traverse
			factor: 		by which value to multiply the parameter
			offset: 		which value to add to the parameter (after multiplication has happended)
			add_items: 		if the parameter is a list, these items are added to the list
			remove_items: 	if the parameter is a list, these items are removed from the list
			set_flags: 		the list of the name of mesonet flags to set
			ignore_missing_item: if remove_items is set and the items named in that list can not be found, a ValueError is raised; this exception can be ignored when this argument is set to True

		Returns:
			dict: the applied factors and offsets.

		Raises:
			ValueError: 	if an item specified in remove_items argument could not be removed from the target list; these errors can be ignored with passing the ignore_missing_item argument
			KeyError: 		if param_name could not be found in the specified objects
		"""

		log.progress("Changing %s of %d %s ...", param_name,
		             self.network.gvs[select_from].num_vertices(), select_from)

		changes = {}

		for vtx in self.network.vertices(select_from=select_from):
			log.debug("Vertex %s ...", vtx)

			vobj = self.network.vobjs[vtx]

			# Check if there is anything to change
			_factor	= dvl.rng.call_if_rng(factor) if factor is not None else None
			_offset = dvl.rng.call_if_rng(offset) if offset is not None else None

			if not (_factor is not None
			        or _offset is not None
			        or remove_items is not None
			        or add_items is not None):
				# Nothing to do for this vertex.
				continue

			# Get the current value and create a new one
			old = dvl.tools.get_via_keylist(vobj, param_name)

			if DEBUG:
				log.debugv("Got value via keylist %s:\n%s", param_name, old)

			if not isinstance(old, list):
				# Assume that it is number-like and can just be handled like that
				log.debugv("Changing %s of %s:  factor %s,  offset %s ...", param_name, vtx, _factor, _offset)

				# Initialise dict ot keep track of the changes
				changes[int(vtx)] = dict(old=old)

				# Initialise new value from old
				new = copy.copy(old)

				if _factor is not None:
					new	*= _factor
					changes[int(vtx)]['factor'] = _factor

				if _offset is not None:
					new += _offset
					changes[int(vtx)]['offset'] = _offset

				changes[int(vtx)]['new'] = new
				# Dnoe.

			else:
				# It is a list, where items can be added or removed
				log.debugv("Changing %s of %s:  add %s,  remove %s ...", param_name, vtx, add_items, remove_items)

				new 	= copy.copy(old)

				if add_items:
					new 	+= add_items
				if remove_items:
					for item in remove_items:
						try:
							new.remove(item)

						except ValueError:
							if not ignore_missing_item:
								log.error("Item %s not present in param %s of %s at vtx %s", item, param_name, vobj.__class__.__name__, vobj.idx, exc_info=True)
								if DEBUG:
									raise

				# Keep track of the changes
				changes[int(vtx)] = dict(old=old, new=new)

			# Set the value
			dvl.tools.set_via_keylist(vobj, param_name, new)

		# If a list of flag names to be set is passed, sets these ...
		if set_flags:
			for flag in set_flags:
				setattr(self.flags, flag, True)
				log.debug("Set %s flag.", flag)

		return [param_name, changes]
	_actn_change_vtx_param.APPEND_RES = True

	# Analysis . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def _actn_analyse_genealogy(self, return_res: bool=False):
		"""Analyses key statistical properties of the genealogy."""
		tree = self.network.genealogy

		birth = tree.vp['birth']
		alive = tree.vp['alive']
		generation = tree.vp['generation']

		# Do some analysis
		max_gen = int(np.max(generation.a))
		mean_gen = float(np.mean(generation.a))
		ratio_alive = float(np.mean(alive.a))
		oldest_alive = self.steps - int(min(birth.a))

		# Set filter to only have alive vertices
		if ratio_alive > 0.:
			tree.set_vertex_filter(alive)

			min_gen_alive = int(np.min(generation.ma))
			max_gen_alive = int(np.max(generation.ma))
			mean_gen_alive = float(np.mean(generation.ma))

			# Clear filters
			tree.clear_filters()
		else:
			# This should only be possible when all are extinct, which should lead to a break condition before this case happens
			min_gen_alive = -1
			max_gen_alive = -1
			mean_gen_alive = np.nan

		# Save to graph properties
		tree.gp['max_gen'] = max_gen
		tree.gp['mean_gen'] = mean_gen
		tree.gp['ratio_alive'] = ratio_alive
		tree.gp['oldest_alive'] = oldest_alive
		tree.gp['min_gen_alive'] = min_gen_alive
		tree.gp['max_gen_alive'] = max_gen_alive
		tree.gp['mean_gen_alive'] = mean_gen_alive

		if return_res:
			# have to work on a copy, otherwise lookups arent possible
			l = copy.copy(locals()) 

			d = {k:l.get(k) for k in tree.gp.keys()}
			d['num_vertices'] = tree.num_vertices()
			d['num_edges'] = tree.num_edges()
			return d

	# Plotting . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def _actn_layout_graph(self, name: str, algo: str, algo_kwargs: dict=None):
		"""Layouts the graph with the given name and saves the layout to the tmp_pos attribute. It can then be read by other methods to have consistent plotting ..."""

		if name == 'genealogy':
			g = self.network.genealogy
		elif name in ['network', 'ResourceNetwork']:
			g = self.network
		else:
			raise ValueError("Invalid graph name "+str(name))

		if not algo_kwargs:
			algo_kwargs = {}

		g.tmp_pos, _ = g._layout_graph(algo=algo, **algo_kwargs)

	def _actn_plot_network(self, output: str=None, output_strf: str=None, **plot_kwargs):
		"""Plots the ResourceNetwork.

		Args:
			tmp_pos_mode (str, None) : if 'read', reads it from the network attribute; if 'save', saves it to the network; if 'both', does both.
		"""

		# Format strf with additional information
		if isinstance(output_strf, str):
			# This removes one layer of {} brackets from the format string
			# NOTE possible to add some information to the filename here
			_output_strf = output_strf.format()
		else:
			_output_strf = output_strf

		self.network.plot(output=output,
		                  output_strf=_output_strf,
		                  **plot_kwargs)

	def _actn_plot_genealogy(self, output: str=None, output_strf: str=None, pt_no: int=None, state_str: str=None, **plot_kwargs):
		"""Plots the Genealogy

		Args:
			tmp_pos_mode (str, None) : if 'read', reads it from the network attribute; if 'save', saves it to the network; if 'both', does both.
		"""

		if self.network.genealogy.num_edges() == 0:
			log.note("No edges in genealogy; skipping plot.")
			return

		# Format strf with additional information
		if isinstance(output_strf, str):
			pt_no = '' if pt_no is None else pt_no
			state_str = '' if state_str is None else state_str

			# This removes one layer of {} brackets from the format string
			_output_strf = output_strf.format(pt=pt_no, state=state_str)

		else:
			_output_strf = output_strf

		self.network.genealogy.plot(output=output,
		                            output_strf=_output_strf, **plot_kwargs)

	# Saving . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def _actn_save_vobj_values(self, nw: str, vp_name: str, select_from: str=None, cast: str=str):
		"""Saves the values of vp_name as a list to the metadata."""

		if nw in ['network', 'rnet']:
			nw = self.network
		elif nw in ['genealogy']:
			nw = self.network.genealogy
		else:
			raise ValueError("Invalid argument nw: "+str(nw))

		if cast in ['float']:
			cast = float
		elif cast in ['int']:
			cast = int

		vals = []

		for vtx in nw.vertices(select_from=select_from):
			vobj = nw.vobjs[vtx]
			vals.append(cast(getattr(vobj, vp_name)))

		return vals
	_actn_save_vobj_values.APPEND_RES = True

	def _actn_save_resource_network(self, fstr: list):
		"""Save the resource network via its internal save method"""
		self.network.save(output_strf=fstr)
		log.note("Saved resource network.")

	def _actn_save_genealogy(self, save_tree: bool=True, save_metadata: bool=True, fstr: list=None):
		"""Save the genealogy via its internal save method"""
		self.network.genealogy.save(output_strf=fstr)
		log.note("Saved genealogy.")


	# Stop Conditions .........................................................
	# To implement project-specific stop conditions, you only need to add private methods here, which have the name pattern _sc_{method_name}, and add the corresponding configuration to cfg.pspace.run_kwargs.stop_kwargs (in your project's config file!) -- it is not necessary to subclass check_stop_conditions.
	# Example below

	def _sc_const_pop_size(self, const_steps: int, tol_val: float=0., mode: str=None):
		"""Check for constant population size over `const_steps`. Optionally, a tolerance for the deviation can be given OR """
		# Only check if enough steps have been simulated
		if self.steps < const_steps:
			return False

		# Get last n entries from monitor
		mon = self.monitors['population_sizes']
		last_n = mon[self.steps-const_steps:self.steps]

		# Convert to signed integer, necessary for diff
		last_n = last_n.astype(int)

		if not mode:
			# Sum up the absolute differences over the steps
			s = np.sum(np.abs(np.diff(last_n, axis=0)), axis=0)
			# NOTE this will fail here, if const_steps is < 2

			# If any value is not zero, s.any() will return True, i.e.: there is a population that is not static. If it returns False, all are static.
			if not s.any():
				# Stop condition fulfilled
				return "sizes {}".format(last_n[-1])

		elif mode in ['abs_slope_leq']:
			# For each population != size 0: Perform a fit and check the slope
			x_vals = list(range(last_n.shape[0]))
			slopes = []

			for col_idx in range(last_n.shape[-1]):
				# Don't fit if already 0
				if last_n[-1, col_idx] == 0.:
					slopes.append(0)
					continue

				# Perform the fit
				fit_res	= scipy.stats.linregress(x_vals, last_n[:, col_idx])
				slopes.append(fit_res[0])
				if abs(fit_res[0]) > tol_val:
					break
			else:
				# All slope smaller tolerance value
				return "slp<{:.2g}: {}".format(tol_val,
				                               ", ".join(["{:.2g}".format(s)
				                                          for s in slopes]))

		elif mode in ['rel_std']:
			# Check via value of the std (relative to mean) over this time
			rel_stds = np.std(last_n, axis=0)/np.mean(last_n, axis=0)

			if np.all(rel_stds[~np.isnan(rel_stds)] < tol_val):
				# All standard deviations are below the tolerance value
				return "σ_rel [{}]".format(", ".join(["{:.2g}".format(s)[1:]
				                           if not np.isnan(s)
				                           else "0"
				                           for s in rel_stds.flat]))

		elif mode in ['abs_std']:
			# Check via absolute value of the std over this time
			stds = np.std(last_n, axis=0)

			if np.all(stds < tol_val):
				# All standard deviations are below the tolerance value
				return "σ [{}]".format(", ".join(["{:.2g}".format(s)
				                                  for s in stds.flat]))

		elif mode in ['acc', 'accumulate']:
			# Calculate using a relative tolerance value

			# Sum up the absolute differences over the steps
			s = np.sum(np.abs(np.diff(last_n, axis=0)), axis=0)
			# NOTE this will fail here, if const_steps is < 2

			# First calculate the accumulated relative deviation per step over the selected number of steps
			rel_dev = s/(np.mean(last_n, axis=0)*const_steps)

			if not rel_dev.shape:
				# has only one element -> reshape to be an iterable
				log.debug("0d-array received, converting...")
				rel_dev = rel_dev.reshape((1,))

			for dev in rel_dev:
				if np.isnan(dev):
					# have to handle these separately as they would always be evaluated to false in smaller/bigger comparisons
					continue
				elif dev > tol_val:
					break
			else:
				# All values are nan or below tolerance -> stop condition
				return "sizes {}".format(last_n[-1])

		else:
			raise ValueError("Invalid mode: "+str(mode))

		# Not stopping
		return False
	_sc_const_pop_size.REQUIRED_MONS = ['population_sizes']

	def _sc_extinction(self):
		"""Check for extinction of all populations"""
		# Get last entry from monitor
		sizes = self.monitors['population_sizes'][self.steps-1:self.steps]

		# Check if they all have size zero
		if not sizes.any():
			# Stop condition fulfilled
			return "all pops extinct"

		# else: there is at least one population that is not of size zero
		return False
	_sc_extinction.REQUIRED_MONS = ['population_sizes']

	def _sc_domination(self, look_back: int=0):
		"""Checks whether a single population was able to displace all others, i.e.: only one population remains.

		`look_back` allows to look at a previous step.
		"""

		# Determine at which step to look
		look_at = max(0, self.steps - look_back)

		# Don't look at initial state
		if look_at == 0:
			return False

		# Get last entry from monitor; have to do this by slice to get a row
		sizes = self.monitors['population_sizes'][look_at-1:look_at].squeeze()

		if len(sizes.shape) == 0:
			# Is already a single population
			return "N_dom={:d} (single pop.)".format(int(sizes))

		# Check if only a single population has size != 0
		if np.sum(sizes > 0) == 1:
			return "N_dom={:d}".format(np.max(sizes))

		# else: no dominating population
		return False
	_sc_domination.REQUIRED_MONS = ['population_sizes']

	def _sc_const_K_frac(self, const_steps: int, rel_tol: float=None):
		"""Check for a constant K_frac value, optionally with a tolerance."""
		# Only check if enough steps have been simulated
		if self.steps < const_steps:
			return False

		# Get last n entries from monitor
		mon = self.monitors['population_carrying_capacity_fraction']
		last_n = mon[self.steps-const_steps:self.steps]

		# Sum up the absolute differences over the steps
		s = np.sum(np.abs(np.diff(last_n, axis=0)), axis=0)
		# NOTE will fail here if const_steps < 2

		if not rel_tol:
			# If any value is not zero, s.any() will return True, i.e.: there is a population that is not static. If it returns False, all all static.
			if not s.any():
				return "K_fracs {}".format(last_n[-1])
		else:
			# Calculate using a relative tolerance value
			# First calculate the accumulated relative deviation over the selected number of steps
			rel_dev = s/(np.mean(last_n, axis=0)*const_steps)

			if not rel_dev.shape:
				# has only one element -> reshape to be an iterable
				log.debug("0d-array received, converting...")
				rel_dev = rel_dev.reshape((1,))

			for dev in rel_dev:
				if np.isnan(dev):
					# have to handle these separately as they would always be evaluated to false in smaller/bigger comparisons
					continue
				elif dev > rel_tol:
					break
			else:
				# All values are nan or below tolerance -> stop condition
				return "K_fracs {}".format(last_n[-1])

		# Not stopping
		return False
	_sc_const_K_frac.REQUIRED_MONS = ['population_carrying_capacity_fraction']

	def _sc_const_pop_engy(self, const_steps: int, rel_tol: float=None):
		"""Check for a constant total population energy (i.e.: of all populations), optionally with a relative tolerance."""

		if self.steps < const_steps:
			return False

		# Get last n entries from monitor
		mon = self.monitors['energy_monitor']
		last_n = mon['population_energy'][self.steps-const_steps:self.steps]

		# Sum up the absolute differences over the steps
		s = np.sum(np.abs(np.diff(last_n, axis=0)), axis=0)
		# NOTE will fail here for const_steps < 2

		if s == 0. or (rel_tol
		               and s/(np.mean(last_n)*const_steps) < rel_tol):
			return "E_pop={:.1e}".format(last_n[-1])

		# else: Not stopping
		return False
	_sc_const_pop_engy.REQUIRED_MONS = ['energy_monitor']

	# Helper methods ..........................................................

	@staticmethod
	def prepare_nw_params(params, *, idx: int, len_iter: int, update_list: list, allow_len_mismatch: bool=False, repeat: bool=False) -> dict:
		"""This methods prepares a parameter dictionary that is used in initialising multiple network objects in an iterated fashion. It allows to update the configuration depending on the current value of `idx`, i.e. the index of the object that the parameters are prepared for. `len_iter` specifies the length of the iteration, i.e. how many objects are added in this fashion."""

		# Resolve potential generators
		params = dvl.rng.resolve_gen(params)

		# Create a copy of the parameters and work on that; all actions that require to work on the same mutable object should be finished by now.
		params = copy.deepcopy(params)

		# See if an update needs to be performed
		if update_list:
			# Perform some checks
			if not allow_len_mismatch and len(update_list) != len_iter:
				raise ValueError("`update_list` did not have the same length "
				                 "as the number of objects that will be "
				                 "added.")

			elif repeat and len(update_list) < len_iter:
				# Extend the update_list with values from the initial update list.
				log.debug("Extending update list to have the same length as "
				          "the number of parameter calls.")
				update_list += [update_list[i%len(update_list)]
				                for i in range(len_iter - len(update_list))]
				# NOTE that this works on the mutable update_list!

			# Now get dictionary that should be used for the update
			update_dict = update_list[idx]

			# If there is something in it, use it to update
			if update_dict:
				params = dvl.tools.rec_update(params, update_dict)

				log.debug("Recursively updated parameters for index %d/%d.",
			              idx, len_iter-1)
			else:
				log.debug("No parameters to update for index %d/%d.",
				          idx, len_iter-1)

		# Done. Return the parameters dictionary.
		return params
