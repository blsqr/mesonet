"""The ResourceNetwork implements the graph on which Mesonet operates."""

import copy

import numpy as np
import graph_tool as gt

import deeevolab as dvl
import deeevolab.network

import mesonet.rnet_objs as rno
import mesonet.genealogy

# Setup logging for this file (inherits from root logger)
log	= dvl.setup_logger(__name__, level=dvl.get_log_level())
cfg	= dvl.get_config(__name__)
log.debug("Loaded module and logger.")

# Local constants
DEBUG = dvl.get_debug_flag()

FLOW_ENABLED = not cfg.disable.flow
if not FLOW_ENABLED:
	log.debug("Not keeping track of flow over edges. You can change this in the rnet_objs config entry.")

# -----------------------------------------------------------------------------

class ResourceNetwork(dvl.network.Network):

	PLOT_STYLES	= cfg.plot_styles

	def __init__(self, *, location=None, max_num_pops: int=None):
		"""Initialise the ResourceNetwork, a directed graph with a certain
		set of vertex and edge properties.
		"""
		log.debug("Initialising ResourceNetwork ...")

		# Initialise from Parent as directed, empty graph_tool.Graph object. The location is used in Network to know where the network is mainly accessed from.
		super().__init__(directed=True, location=location)

		# Initialise Vertex property maps
		# type:   explicit encoding
		# is_***: one-hot encoding
		self.vp['is_res']			= self.new_vp('bool')
		self.vp['is_res1']			= self.new_vp('bool')
		self.vp['is_res2']			= self.new_vp('bool')
		self.vp['is_pop']			= self.new_vp('bool')
		self.vp['is_not_pop']		= self.new_vp('bool')
		self.vp['is_not_aux']		= self.new_vp('bool')
		self.vp['type'] 			= self.new_vp('string')

		# energy-related
		self.vp['energy'] 			= self.new_vp('float')
		self.vp['energy_scale']		= self.new_vp('float')
		self.vp['delta_energy']		= self.new_vp('float')
		self.vp['energy_in']		= self.new_vp('float')
		self.vp['energy_out']		= self.new_vp('float')

		# for populations
		self.vp['size']				= self.new_vp('int')
		self.vp['K_frac']			= self.new_vp('float')
		self.vp['birth']			= self.new_vp('int')
		self.vp['demise']			= self.new_vp('int')
		self.vp['alive']	 		= self.new_vp('bool')
		self.vp['num_innovs']	 	= self.new_vp('int')
		self.vp['expl_eff']		 	= self.new_vp('float')
		# other population attributes are part of the NetworkObjects

		# for network topology
		self.vp['num_roots'] 		= self.new_vp('int') # i.e.: in_degree
		self.vp['num_leaves'] 		= self.new_vp('int') # active out_degree

		# Initialise Edge property maps
		self.ep['inactive']			= self.new_ep('bool')
		self.ep['flow'] 			= self.new_ep('float')
		self.ep['max_flow'] 		= self.new_ep('float')
		self.ep['efficacy'] 		= self.new_ep('float')


		# Initialise Graph property maps
		self.gp['energy_propagated']= self.new_gp('float')
		self.gp['total_energy'] 	= self.new_gp('float')
		self.gp['system_energy'] 	= self.new_gp('float')
		self.gp['resource_energy']	= self.new_gp('float')
		self.gp['population_energy']= self.new_gp('float')
		self.gp['max_num_pops']		= self.new_gp('int')

		# Make some Graph property maps accessible via attributes
		self.gp['max_num_pops'] = 0 if not max_num_pops else max_num_pops

		# Initialise GraphViews to access different types of vertices easily
		# Resources
		self.gvs['res']  = gt.GraphView(self, vfilt=self.vp['is_res'])
		self.gvs['res1'] = gt.GraphView(self, vfilt=self.vp['is_res1'])
		self.gvs['res2'] = gt.GraphView(self, vfilt=self.vp['is_res2'])

		# Populations and the inverse
		self.gvs['pops'] 	 = gt.GraphView(self, vfilt=self.vp['is_pop'])
		self.gvs['alive'] 	 = gt.GraphView(self, vfilt=self.vp['alive'])
		self.gvs['not_pops'] = gt.GraphView(self, vfilt=self.vp['is_not_pop'])

		# Everything except auxilary (i.e.: without EnergySink, EnergySource)
		self.gvs['not_aux']	= gt.GraphView(self, vfilt=self.vp['is_not_aux'])

		# With edge filtering
		self.gvs['res2_active']	= gt.GraphView(self.gvs['res2'])
		self.gvs['res2_active'].set_edge_filter(self.ep['inactive'],
		                                        inverted=True)

		# Set up the Genealogy
		self.genealogy = mesonet.genealogy.Genealogy(location=self.location,
		                                             vtx_link=self)


		# Create temporary PropertyMaps that are needed for calculations in energy propagation. They are reset when they are needed.
		self._tmp_vp = {}
		self._tmp_vp['dE'] 		= self.new_vp('float')
		self._tmp_vp['E_in'] 	= self.new_vp('float')
		self._tmp_vp['E_out'] 	= self.new_vp('float')

		self._tmp_ep = {}
		self._tmp_ep['flow'] 	= self.new_ep('float')

		# Set up other attributes that will be filled later
		self.src_vtx = None
		self.sink_vtx = None

		log.debug("%s initialised.", self.__class__.__name__)

	# Building the Network ....................................................
	# Adding vertices . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def add_source_and_sink(self, const_flow: float=False):
		"""Add a EnergySource and EnergySink to the network.
		
		Args:
		    const_flow (float, optional): if given, a ConstantFlowEnergySource
		    	is used instead of an EnergySource
		"""
		# Check if source and/or sink were already created
		if self.src_vtx or self.sink_vtx:
			raise RuntimeError("Source and or sink vertices were already set!")

		# Create the vertices
		src_vtx = self.add_vertex()
		sink_vtx = self.add_vertex()

		# Create the corresponding objects
		# Energy source
		if const_flow is False:
			src_obj	= rno.EnergySource(g=self, desc=src_vtx)
		else:
			src_obj	= rno.ConstantFlowEnergySource(g=self, desc=src_vtx,
			                                       const_flow=const_flow)

		# Energy sink
		sink_obj = rno.EnergySink(g=self, desc=sink_vtx)

		# Associate objects with network property maps
		self.vobjs[src_vtx] = src_obj
		self.vobjs[sink_vtx] = sink_obj

		# Also add attributes
		self.src_vtx = src_vtx
		self.sink_vtx = sink_vtx

	def add_primary_resource(self, **res1_kwargs):
		"""Add a primary resource to the network."""
		# Create vertex and PrimaryResource as NetworkObject
		vtx = self.add_vertex()
		res = rno.PrimaryResource(g=self, desc=vtx, **res1_kwargs)

		# Associate objects or values with network property maps
		self.vobjs[vtx]	= res

		# Done here -- primary resources have no incoming connections
		return vtx, res

	def add_secondary_resource(self, *, energy_input: float=0., res1_roots: dict=None, input_efficacy: float=1., **res2_kwargs):
		"""Add a secondary resource to the network."""
		# Create vertex and resource
		vtx = self.add_vertex()
		res = rno.SecondaryResource(g=self, desc=vtx,
		                            energy_input=energy_input, **res2_kwargs)

		# Connect the energy source to this resource
		try:
			self.connect_source_to(vtx, max_flow=energy_input,
		    	                   efficacy=input_efficacy)
		except ValueError as err:
			log.error("Could not connect secondary resource %s to source "
			          "vertex due to a ValueError: %s! Vertex will be removed "
			          "again.", res, err)

			# Remove the vertex (no NetworkObject associated with it yet)
			self._remove_vertex(vtx)
			raise

		# Associate objects or values with network property maps
		self.vobjs[vtx]	= res

		# Add connections
		if res1_roots:
			res.add_roots(select_from='res1', **res1_roots)

		return vtx, res

	def add_population(self, *, src_root: dict=None, res1_roots: dict=None, res2_roots: dict=None, **pop_kwargs):
		"""Add a population to the network."""
		# Create vertex in ResourceNetwork and the Population object and make the object available to the ResourceNetwork
		vtx = self.add_vertex()
		pop = rno.Population(g=self, desc=vtx, **pop_kwargs)

		# Try to connect the population to the energy sink with a flow rate
		# equal to the population's energy need per individual
		try:
			self.connect_to_sink(vtx, max_flow=pop.energy_need)
		
		except ValueError as err:
			log.error("Could not connect population %s to sink vertex due to "
			          "a ValueError: %s! Vertex will be removed again.",
			          pop, err)

			# Remove the vertex (no NetworkObject associated with it yet)
			self._remove_vertex(vtx)
			raise

		# Add connections
		if src_root and src_root.pop('connect', False):
			self.connect_source_to(vtx, **src_root)

		if res1_roots:
			pop.add_roots(select_from='res1', **res1_roots)

		if res2_roots:
			pop.add_roots(select_from='res2', **res2_roots)

		# Associate it
		self.vobjs[vtx]	= pop

		# Created all the connections and adjusted all parameters. Now can create a vertex in the genealogy and create a link to and from the population object
		gvtx = self.genealogy.add_vertex(linked_vtx=vtx, linked_vobj=pop)
		pop.add_link(g=self.genealogy, desc=gvtx)

		return vtx, pop

	# Adding / removing edges . . . . . . . . . . . . . . . . . . . . . . . . .

	def connect_source_to(self, vtx, *, max_flow: float, efficacy: float=1.):
		"""Connects the given vertex to the source vertex."""
		if not self.src_vtx:
			raise ValueError("Source vertex was not set yet!")

		log.debug("Connecting vertex %s to energy source.", vtx)
		e = self.add_edge(source=self.src_vtx, target=vtx)

		# Set the edge properties
		self.ep['max_flow'][e] = max_flow
		self.ep['efficacy'][e] = efficacy

		return e

	def connect_to_sink(self, vtx, *, max_flow: float, efficacy: float=1.):
		"""Connects the given vertex to the sink vertex."""
		if not self.sink_vtx:
			raise ValueError("Sink vertex was not set yet!")

		log.debug("Connecting vertex %s to energy sink.", vtx)
		e = self.add_edge(source=vtx, target=self.sink_vtx)

		# Set the edge properties
		self.ep['max_flow'][e] = max_flow
		self.ep['efficacy'][e] = efficacy

		return e

	def remove_edge(self, edge):
		"""Remove an edge from the ResourceNetwork.

		Note that this method is not available in the parent class method, as it might lead to invalidated Edge descriptors. This class, however, assumes that no Edge descriptors are stored, and thus re-enabled the method."""
		return super(dvl.network.Network, self).remove_edge(edge)

	# Genealogy . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def manual_update_genealogy(self) -> None:
		"""Manually transfers some information from the ResourceNetwork to the Genealogy. This might be required after an action was performed which changed the state of the NetworkObjects or the ResourceNetwork but not of the genealogy, e.g.: changing a population parameter."""

		# Loop over all vertices
		for vtx in self.vertices(select_from='pops'):
			vobj = self.vobjs[vtx]

			# Set the attributes of genealogy from the population's attributes
			vobj.set_link_item('p_innov', vobj.p_innov)
			vobj.set_link_item('split_size', vobj.split_size)
			# ... can have more here

	# Dynamics ................................................................

	def propagate_energy(self, select_from: str=None, vtx_list: list=None):
		"""Propagates energy inside the resource network.

		Current strategy: round-based simultaneous, meaning each energy receiver gets energy from the respective energy sources at the same time. If not enough energy is available, the energy is split according to the transfer rates.

		Possible extensions:
		- Take `strategy` argument and use it to change the energy propagation mechanism.
		"""

		log.info("Propagating energy in network ...")

		# Get a vertex iterator
		vtx_it = self.vertices(select_from=select_from, vtx_list=vtx_list)

		# Get the temporary property maps
		dE = self._tmp_vp['dE']
		tmp_E_in = self._tmp_vp['E_in']
		tmp_E_out = self._tmp_vp['E_out']
		tmp_flow = self._tmp_ep['flow']

		# Reset dE
		dE.a = np.zeros(len(dE.a))
		# NOTE the temporary PropertyMap dE is needed, because delta_energy fulfills only the purpose of monitoring the change in energy *in one simulation step*. This is not an issue if all vertices are propagated at once, but if some are propagated on their own, the values in delta_energy will contribute more than once. Thus, a separate dE property map is needed.

		# Loop over all Vertices
		for vtx in vtx_it:
			# Call the vertex object to propagate its energy
			_dE = self.vobjs[vtx].propagate_energy(energy_in=tmp_E_in,
			                                       energy_out=tmp_E_out,
			                                       flow=tmp_flow)
			if _dE is not None:
				dE.a += _dE
			# else: the propagation condition was not fulfilled
			# NOTE that the temporary property maps are reset by the Vertex Object; thus, they are only being reset, if the propagation condition is fulfilled, which improves runtime.

		# Apply the change in energy to the energy property map, i.e.: propagate the energy inside the network.
		self.vp['energy'].a += dE.a
		# NOTE this ignores the VertexObjects' property methods

		log.note("Finished propagating energy.")

	def population_dynamics(self):
		"""For all population vertices, call their population dynamics"""
		log.info("Performing population dynamics on %d populations ...",
		         self.gvs['pops'].num_vertices())

		for pop_vtx in self.gvs['pops'].vertices():
			log.progress("Population vertex %s ...", pop_vtx)

			self.vobjs[pop_vtx].population_dynamics()

		log.note("Finished population dynamics.")

	def population_splitting(self):
		"""Loops over all populations and checks if their size is larger than
		the designated splitting size.

		If so, a new vertex with the same connections and properties as this
		population is created in the network.

		If mutation rates (defined in the populations' split_kwargs attribute)
		are >0, there can be mutations during the copying procedure ...
		"""

		log.info("Checking whether populations need to split ...")

		for pop_vtx in self.gvs['pops'].vertices():
			log.progress("Population %s", pop_vtx)

			pop	= self.vobjs[pop_vtx]

			if not pop.split_size or pop.size <= pop.split_size:
				# Not splitting
				continue

			log.debugv("%d > %d -> splitting ...", pop.size, pop.split_size)

			# Copy the vertex and the associated VertexObject, then get the
			# VertexObject for the new vertex
			new_pop_vtx = self.copy_vertex(pop_vtx)
			new_pop = self.vobjs[new_pop_vtx]
			# NOTE The links of the new Population object are now invalidated

			# Keep track of the hereditary connection, by adding a tuple of
			# (step, parent index) to the ancestors attribute of the new
			# Population object
			ancestors = copy.deepcopy(pop.ancestors)
			ancestors.append((pop.idx, pop.get_linked_idcs()[0]))
			new_pop.ancestors = ancestors
			# NOTE that this assumes that only the genealogy is linked (list index 0)

			# Get the kwargs for splitting and mutation
			split_kwargs = pop.split_kwargs

			# Determine split factor
			split_factor = dvl.rng.call_if_rng(split_kwargs.get('split_factor', .5))

			# Adjust both to the new population sizes and energies
			new_pop.size = np.floor(pop.size * (1. - split_factor))
			pop.size = np.ceil(pop.size * split_factor)

			new_pop.energy = pop.energy * (1. - split_factor)
			pop.energy = pop.energy * split_factor

			# Set the birth time
			new_pop.birth = self.location.steps

			# Perform the mutations
			if split_kwargs.get('mutations'):
				log.debugv("Mutating new population ...")
				new_pop.receive_mutations()

			# Update the num_roots, num_leaves
			new_pop.num_roots = new_pop.in_degree()

			for res_vtx, _ in new_pop.in_edges():
				self.vp['num_leaves'].a[int(res_vtx)] += 1

			# Now that the ancestry is updated and the mutations have been performed, the vertex in the genealogy can be created and the previously invalidated links can be recreated
			gvtx = self.genealogy.add_vertex(linked_vtx=new_pop_vtx,
			                                 linked_vobj=new_pop)
			new_pop.add_link(g=self.genealogy, desc=gvtx)
			# NOTE This will also write the values from the Population object to the genealogy; it is therefore important, that all further changes are manually kept up to date in the genealogy.

			# Done with this population.

		# Check if the maximum number of populations was reached
		if (self.gp['max_num_pops']
		    and self.gvs['pops'].num_vertices() > self.gp['max_num_pops']):
			# TODO
			raise NotImplementedError

			# log.note("Reducing number of populations to maximum number ...")
			# log.state("Currently %d populations, target %d.",
			#           self.gvs['pops'].num_vertices(), max_num_pops)


		log.note("Finished splitting procedures.")

	def add_innovations(self):
		"""Loop over vertices and call their add_innovation method"""
		log.info("(Possibly) adding innovations ...")

		for pop_vtx in self.gvs['pops'].vertices():
			log.progress("Population %s", pop_vtx)

			self.vobjs[pop_vtx].add_innovation()

		log.note("Finished innovation procedures.")

	# Helpers .................................................................

	def reset_energy_changes(self):
		"""Resets the property maps energy_in, energy_out, and delta_energy."""
		self.vp['energy_in'].set_value(0.)
		self.vp['energy_out'].set_value(0.)
		self.vp['delta_energy'].set_value(0.)
		self.vp['expl_eff'].set_value(0.)
		self.ep['flow'].set_value(0.)

	def calc_global_energy_measures(self):
		"""Calculate and save some energy measures to graph properties."""

		log.progress("Calculating global energy measures ...")

		gp = self.gp

		gp['total_energy'] = np.sum(self.vp['energy'].a)
		gp['system_energy']	= np.sum(self.vp['energy'].a[2:])
		gp['resource_energy'] = np.sum(self.gvs['res'].vp['energy'].ma)
		gp['population_energy'] = np.sum(self.gvs['pops'].vp['energy'].ma)
		gp['energy_propagated'] = np.sum(np.abs(self.vp['delta_energy'].a))/2

		log.state("Total energy:        %.2f", gp['total_energy'])
		log.state("System energy:       %.2f", gp['system_energy'])
		log.state("Resource energy:     %.2f", gp['resource_energy'])
		log.state("Population energy:   %.2f", gp['population_energy'])
		log.state("Energy propagated:   %.2f", gp['energy_propagated'])

	# Plotting ................................................................
	# Extensions of what is defined in the parent class
