"""Children of VertexObject and EdgeObject that are used to associate vertices and edges with Resources, Populations, Innovations, ..."""

import math
import random
from functools import partial
from typing import Union

import numpy as np
from easydict import EasyDict as DotDict

import graph_tool as gt

import deeevolab as dvl
from deeevolab.rng import call_if_rng, RNG
from deeevolab.network import VertexObject, EdgeObject

# Setup logging for this file (inherits from root logger)
log	= dvl.setup_logger(__name__, level=dvl.get_log_level())
cfg = dvl.get_config(__name__)
log.debug("Loaded module and logger.")

# Local constants
ZERO_TOL = cfg.zero_tol
DEBUG = dvl.get_debug_flag()

FLOW_ENABLED = not cfg.disable.flow
if not FLOW_ENABLED:
	log.debug("Not keeping track of flow over edges. You can change this in the rnet_objs config entry.")

# For convenient type hints
PropertyMap = gt.PropertyMap

# -----------------------------------------------------------------------------

class Resource(VertexObject):

	# __slots__ = () # additional slots # FIXME should not be necessary
	__slots__ = ('g', 'desc', '_pdict', 'idx', '_linked')
	_properties = ('energy', 'propagation_condition')

	def __init__(self, *nwo_args, energy: float, energy_scale: float=None, **nwo_kwargs):
		"""Initialise a Resource object.
		
		Args:
		    *nwo_args, **nwo_kwargs: passed on to parent VertexObject
		    energy (float): the initial energy of the resource
		    energy_scale (float, optional): the scale for the energy values;
		    	this quantity is used in the dynamics and represents behaviour 
		    	akin to the reaching of a "fill level".
		
		"""

		super().__init__(*nwo_args, **nwo_kwargs)

		# Set up the managed attributes
		self.energy_scale 	= energy_scale if energy_scale else np.nan
		self.energy 		= energy

		# Set other attribute values
		self.delta_energy 	= 0.
		self.alive 			= False # can only be True for populations
		self.is_not_aux		= True # the default; only Source and Sink are aux

	@property
	def energy(self):
		"""The energy currently stored in this resource."""
		return self['energy']

	@energy.setter
	def energy(self, val):
		if val < 0.:
			if val < ZERO_TOL:  # ZERO_TOL is a negative value!
				raise ValueError("Energy needs to be positive, but it was "
				                 "tried to set it to {} in {} with associated "
				                 "vertex {}"
				                 "".format(val, self.__class__.__name__,
				                           self.desc))
			else:
				log.caution("Below zero value within tolerance encountered; "
				            "%s < %s. Setting to 0.", val, ZERO_TOL)
				val = 0.

		else:
			self['energy'] = val

	@property
	def propagation_condition(self):
		"""Determines, whether there will be energy propagation _away from_ this vertex.

		NOTE It is important to let this be a property, because the energy is not always set using the property setter and thus no checks can be performed on whether the propagation condition is fulfilled or not.
		"""
		return bool(self.energy > 0.)

	# Methods .................................................................

	def add_roots(self, *, max_flow: float, efficacy: float=1., num: int=None, strategy: str='random', strategy_kwargs: dict=None, select_from: str=None) -> list:
		"""Add roots (i.e., in-neighbours) to this resource using a strategy.
		
		Args:
		    max_flow (float): The maximum flow over this edge
		    efficacy (float, optional): The efficacy of this edge; default: 1.0
		    num (int, optional): The number of roots to add. With the strategy
		    	`all`, this need not be given; otherwise it is needed. 
		    strategy (str, optional): The name of the strategy to find roots
		    strategy_kwargs (dict, optional): kwargs passed to the strategy
		    select_from (str, optional): Which graph view to select from
		
		Returns:
		    list: newly added roots
		
		Raises:
		    ValueError: Invalid combination of `num` argument and strategy
		"""
		if num is None and strategy != 'all':
			raise ValueError("Need a value for argument `None` for all "
			                 "strategies except 'all'.")

		log.debugv("Connecting to %s roots from GraphView '%s' with "
		           "strategy '%s'.", num, select_from, strategy)

		# Choose out of these vertices using the given strategy
		_sk = strategy_kwargs if strategy_kwargs else {}
		roots = self.vtx_finder(strategy=strategy, select_from=select_from,
		                        num=num, **_sk)

		if roots is None:
			# Nothing to add here, return empty list
			return []

		# Create edges
		edges = [self.g.add_edge(source=rt, target=self.idx) for rt in roots]

		# TODO Add Innovation objects?!

		# Set edge properties
		for e in edges:
			if max_flow:
				self.g.ep['max_flow'][e] = call_if_rng(max_flow)
			if efficacy:
				self.g.ep['efficacy'][e] = call_if_rng(efficacy)

			# Set corresponding vertex properties of source and target vertex
			s, t = e  # source, target
			self.g.vp['num_leaves'].a[int(s)] += 1
			self.g.vp['num_roots'].a[int(t)] += 1

		# Done. Return the list of roots
		return roots

	# Dynamics ................................................................

	def propagate_energy(self, *, energy_in: PropertyMap, energy_out: PropertyMap, flow: PropertyMap) -> PropertyMap:
		"""Propagates the energy from this resource to the out_neighbours.
		
		How much energy is transferred depends on the _calc_energy_outflow
		method that each resource can adapt.
		
		NOTE: Important! the passed PropertyMaps are temporary and need to be
		reset before they can be used for calculations. They are created in
		the calling method, because that allows to store them and not having
		to recreate them constantly.
		FIXME in the long term: find a way around this. It is not very elegant
		to require external property maps for this. Maybe getting them via
		self.g._tmp_vp ... is ok?
		
		Args:
		    energy_in (PropertyMap): temporary vertex property map to gather
		    	the energy influx in
		    energy_out (PropertyMap): temporary vertex property map to gather
		    	the energy outflux in
		    flow (PropertyMap): temporary edge property map to gather the
		    	energy flow in
		
		Returns:
		    PropertyMap: The changes in vertex energies that originate from
		    	_this_ resource. These need to be accumulated by a wrapper
		    	function to get the total change per time step.
		"""

		# Check if there should be propagation
		if not self.propagation_condition:
			if DEBUG:
				log.debugv("Propagation condition not fulfilled for %s "
				           "(vtx %s) ...", self.__class__.__name__, self.idx)
			return

		log.debugv("Propagating energy from %s (vtx %s) ...",
		           self.__class__.__name__, self.idx)

		# Reset temporary property maps
		energy_in.a = np.zeros(len(energy_in.a))
		energy_out.a = np.zeros(len(energy_out.a))
		# NOTE setting array directly is much faster than .set_value(0.)

		if FLOW_ENABLED:
			# only do this if necessary; shaves off a few percent runtime
			flow.a = np.zeros(len(flow.a))

		# Initialise some variables
		dE_tot = 0.
		inactive = self.g.ep['inactive']
		max_flows = self.g.ep['max_flow']
		efficacies = self.g.ep['efficacy']

		# Loop over all out-edges and record the energy change
		for src_vtx, target_vtx, edge in self.out_edges(with_idx=True):
			# NOTE self.idx is equal to the src_vtx

			if inactive.a[edge]:
				# Reached inactive edge, e.g. when a population deceased
				continue

			target = self.get_vobj(target_vtx)

			# TODO also get the innovation VertexObject

			# Get the flow rate of this edge and the max energy flow
			max_flow = max_flows.a[edge]
			efficacy = efficacies.a[edge]

			# Calculate the energy outflow
			dE = self._calc_energy_outflow(target, max_flow=max_flow,
			                               efficacy=efficacy)

			if DEBUG:
				log.debugv("-> %s (vtx %d):\n"
				           "\t\t\t  max_flow: %-10.2f    efficacy: %.3f\n"
				           "\t\t\t  energy:    %-10.2f    dE:         %-5.3f",
				           target.__class__.__name__, target.idx,
				           max_flow, efficacy,
				           self.energy, -dE)

			# Apply energy change to the temporary PropertyMaps and keep track of total energy spent
			dE_tot 					+= dE
			energy_out.a[src_vtx]	+= dE
			energy_in.a[target_vtx]	+= dE

			if FLOW_ENABLED:
				flow.a[edge] = dE

		# Calculate scaling -- takes care of cases where dE is larger than the available energy
		scaling = self._calc_energy_scaling(dE_tot)

		# Apply the temporary PropertyMaps to the actual ones
		if scaling is None:
			# Calculate the values without scaling
			self.g.vp['energy_in'].a += energy_in.a
			self.g.vp['energy_out'].a += energy_out.a

			ddE	= (energy_in.a - energy_out.a)
			# NOTE: it is slightly faster without scaling here; makes a difference because it is called frequently without scaling.

		else:
			# Scale everything down by the calculated factor
			self.g.vp['energy_in'].a += energy_in.a * scaling
			self.g.vp['energy_out'].a += energy_out.a * scaling

			ddE	= (energy_in.a - energy_out.a) * scaling

		# Apply to delta_energy to keep track of the changes in this iteration
		self.g.vp['delta_energy'].a += ddE
		# NOTE delta_energy is *not* the one that is being used to apply the energy changes; the accumulated return value of this method is.

		if FLOW_ENABLED:
			if scaling is None:
				self.g.ep['flow'].a	+= flow.a
			else:
				self.g.ep['flow'].a	+= flow.a * scaling
		# NOTE: the irrelevant vertices or edges have value 0, thus allowing these operations

		# Return the property map array of the changes in energy originating from _this_ vertex. In the wrapper, they are accumulated and finally applied to the global Energy PropertyMap
		return ddE

	def _calc_energy_outflow(self, target: VertexObject, *, max_flow: float, efficacy: float) -> float:
		"""Calculate the energy outflow from this resource to a target, given
		the edge properties (max_flow and efficacy) of the connecting edge.
		
		Args:
		    target (VertexObject): The target object
		    max_flow (float): The max_flow over the edge connecting these two
		    efficacy (float): The efficacy of the edge connecting these two
		
		Returns:
		    float: The energy outflow from this resource
		
		Raises:
		    NotImplementedError: If the outflow goes to a non-population vertex
		"""

		if target.is_pop:			
			# Calculate energy outflow according to efficacy and resource fill level (i.e.: energy/energy scale)
			ee = (1. - math.exp(-efficacy * self.energy/self.energy_scale))
			dE = target.size * max_flow * ee

			# Save the exploitation efficiency to the target
			target.expl_eff += ee
			return dE

		# Is a resource, probably a secondary one
		# dE = self.energy * max_flow * efficacy
		# TODO this should also depend on the target's fill level
		raise NotImplementedError("outflow to non-population vertices")

	def _calc_energy_scaling(self, dE: float) -> Union[float, None]:
		"""Check if more energy would be spent than present in this resource.
		
		If yes, the scaling factor is calculated and returned.
		Else, None is returned.
		
		Args:
		    dE (float): The initially calculated change in energy
		
		Returns:
		    float: The scaling factor, if `dE > self.energy`; else None.
		"""
		if dE > self.energy:
			# Scale the PropertyMaps by the ratio
			s = self.energy / dE

			log.debugv("Energy depletion encountered for vtx %s. Scaling by "
			           "factor %f ...", self.idx, s)
			return s

		return None

	# Analysis ................................................................
	# ...

	# Root finding strategies .................................................

	def vtx_finder(self, strategy: str, num: int=1, shuffle: bool=None, with_replace: bool=False, **strategy_kwargs) -> list:
		"""Given a strategy name, returns a list of vertices.

		There is some advanced behaviour for the `num` argument:
		* `num` can be reduced to the number of vertices deemed fitting by the
		  	specified strategy.
		*  For num < 0, that number is deduced from the total number of
			possible vertices, such that with 11 possible vertices and 
			`num == -4`, 7 vertices are returned.
		* If num is larger than the number of available vertices, it is reduced
			to that number.
		
		Args:
		    strategy (str): The name of the strategy to use for finding
		    	vertices; it needs to be a method of this class that is
		    	prefixed with `_strategy_`
		    num (int, optional): The number of vertices to find
		    shuffle (bool, optional): Whether to shuffle the resulting list
		    with_replace (bool, optional): Whether to shuffle with replace
		    **strategy_kwargs: Passed to the strategy
		
		Returns:
		    list: The vertices found by the given strategy
		"""

		# Need not look further if no vertices are requested
		if num == 0:
			return None

		# Ensure "random" and "by_index" have reasonable defaults
		if shuffle is None:
			if strategy in ['random', 'random_maxdist']:
				shuffle = True
			elif strategy == 'by_index':
				shuffle = False
			else:
				shuffle = True

		# Get the strategy method and call it to get a list of fitting vertices
		strategy_method = getattr(self, '_strategy_' + strategy)
		vtcs = strategy_method(**strategy_kwargs)

		# Ensure strategy "all" works as desired
		if strategy == 'all':
			num = len(vtcs)

		if not vtcs:
			# No vertices were available
			log.debug("vtx_finder, strategy '%s': No vertices found.",
			          strategy)
			return None

		if num > len(vtcs):
			# More vertices required than available; reduce to that number
			log.debug("vtx_finder: more vertices requested than available. "
			          "Only returning %d vertices, not %d.\nIf this behaviour "
			          "is unwanted, reduce the corresponding config "
			          "parameter." , len(vtcs), num)
			num = len(vtcs)

		elif num < 0:
			# Set the number relative to the number of possible vertices
			log.debug("vtx_finder: got negative number -> deducing from total number of possible vertices.")
			num = len(vtcs) + num

			if num <= 0:
				log.debugv("Got below 0, not returning any vertices.")
				return None

		if shuffle:
			# Choose a number of elements randomly out of the list
			return list(np.random.choice(vtcs, size=num, replace=with_replace))

		# Return the first num elements -- as a list!
		return vtcs[:num]

	def _strategy_all(self, *, select_from: str='res') -> list:
		"""Returns all vertices that match the `select_from` group."""
		return list(self.g.vertices(select_from=select_from))

	def _strategy_by_index(self, *, vtx_list: list, select_from: str='res') -> list:
		"""Select vertices by the indices supplied in the `vtx_list` argument."""
		return list(self.g.vertices(select_from=select_from,
		                            vtx_list=vtx_list))

	def _strategy_random(self, *, select_from: str='res', vtx_list: list=None) -> list:
		"""Selects all vertices and vtx_finder will select randomly from it."""
		return list(self.g.vertices(select_from=select_from,
		                            vtx_list=vtx_list))

	def _strategy_random_maxdist(self, *, max_dist: int=2, select_from: str='res', vtx_list: list=None) -> list:
		"""Selects a random vertex that is in the maximal distance from the current vertex"""
		raise NotImplementedError("Needs to be fixed!")
		# vtcs = self.g.vertices(select_from=select_from, vtx_list=vtx_list)

		# # Calculate the distance of these vertices and gather those that are <= dist away from this vertex
		# # This ignores the directionality of the Graph. Therefore, all secondary resources are next neighbors to each other via the EnergySource. Thus, the EnergySource and EnergySink need to be ignored.
		# g = self.g.gvs['not_aux']
		# dfunc = lambda t: gt.topology.shortest_distance(g, source=self.desc,
		#                                                 target=t,
		#                                                 directed=False,
		#                                                 max_dist=max_dist)

		# # Find all vertices within desired distance and return
		# return [vtx for vtx in vtcs if dfunc(vtx) <= max_dist]

	def _strategy_largest_energy(self, *, select_from: str='res', vtx_list: list=None) -> list:
		"""Selects the vertex with the highest energy."""
		vtcs = self.g.vertices(select_from=select_from, vtx_list=vtx_list)

		# Compile dict of all energies
		energies = {vtx:self.g.vobjs[vtx].energy for vtx in vtcs}
		max_E = max(energies.values())

		# Find all vertices with largest energy and return
		return [vtx for vtx, v in energies.items() if v == max_E]

	def _strategy_highest_input(self, *, select_from: str='res2', vtx_list: list=None) -> list:
		"""Selects the vertex with the highest energy input"""
		vtcs = self.g.vertices(select_from=select_from, vtx_list=vtx_list)

		# Compile dict of all VertexObjects' energy input
		energy_in = {vtx:self.g.vobjs[vtx].energy_in for vtx in vtcs}

		# Find maximum input
		max_input = max(energy_in.values())

		# Find vertices corresponding to highest input and return
		return [vtx for vtx, v in energy_in.items() if v == max_input]

	def _strategy_lowest_input(self, *, select_from: str='res2', vtx_list: list=None, num: int=1, replace: bool=False) -> list:
		"""Selects the vertex with the lowest energy input"""
		vtcs = self.g.vertices(select_from=select_from, vtx_list=vtx_list)

		# Compile dict of all VertexObjects' energy input
		energy_in = {vtx:self.g.vobjs[vtx].energy_in for vtx in vtcs}

		# Find minimum input
		min_input = min(energy_in.values())

		# Find vertices corresponding to lowest input and return
		return [vtx for vtx, v in energy_in.items() if v == min_input]

	def _strategy_highest_output(self, *, select_from: str='res', vtx_list: list=None) -> list:
		"""Selects the vertex with the highest energy output"""
		vtcs = self.g.vertices(select_from=select_from, vtx_list=vtx_list)

		# Compile dict of all VertexObjects' energy output
		energy_out = {vtx:self.g.vobjs[vtx].energy_out for vtx in vtcs}

		# Find maximum output
		max_output = max(energy_out.values())

		# Find vertices corresponding to highest output and return
		return [vtx for vtx, v in energy_out.items() if v == max_output]

	def _strategy_lowest_output(self, *, select_from: str='res', vtx_list: list=None) -> list:
		"""Selects the vertex with the lowest energy output"""
		vtcs = self.g.vertices(select_from=select_from, vtx_list=vtx_list)

		# Compile dict of all VertexObjects' energy output
		energy_out = {vtx:self.g.vobjs[vtx].energy_out for vtx in vtcs}

		# Find minimum output
		min_output = min(energy_out.values())

		# Find vertices corresponding to lowest output and return
		return [vtx for vtx, v in energy_out.items() if v == min_output]

	def _strategy_least_out_conns(self, *, select_from: str='res', vtx_list: list=None):
		"""Selects the vertex with the lowest number of outgoing connections"""
		vtcs = self.g.vertices(select_from=select_from, vtx_list=vtx_list)

		# Compile dict of all VertexObjects' out degree
		k = {vtx:self.g.vobjs[vtx].out_degree() for vtx in vtcs}
		# NOTE important to not use a filter or GraphView here, as then the out degree is also affected by it and it would make no sense to use the same filter as used in determining the list of vertices

		# Find minimum degree
		min_k = min(k.values())

		# Find vertices corresponding to lowest out degree and return
		return [vtx for vtx, _k in k.items() if _k == min_k]

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class EnergySource(Resource):
	"""The EnergySource is where energy is brought into the network from the
	outside.

	The EnergySource keeps track of its own energy by going to negative energy
	values; this is the only Resource-derived object where this is allowed.
	"""

	__slots__ = ('g', 'desc', '_pdict', 'idx', '_linked')

	propagation_condition = True # Always propagate from EnergySource

	def __init__(self, *args, **kwargs):
		"""Initialise an EnergySource.

		An energy source is always initialised with zero stored energy.
		
		Args:
		    *args: Passed to Resource.__init__
		    **kwargs: Passed to Resource.__init__
		"""

		super().__init__(*args, energy=0., **kwargs)

		self.type 			= 'energy_src'
		self.is_not_aux 	= False
		self.is_not_pop 	= True
		self.size 			= -1 # i.e.: not applicable

	@property
	def energy(self) -> float:
		"""The current energy of the EnergySource.

		Once energy was propagated from this object, the will be a negative
		value because 
		
		Returns:
		    float: The current energy of this EnergySource 
		"""
		return self['energy']

	@energy.setter
	def energy(self, val: float):
		"""Set the energy of the energy source to the given value.

		This lifts the restriction by the parent class the resources need to
		have a positive energy level.
		"""
		self['energy'] = val

	@property
	def available_flow(self) -> float:
		"""If using the EnergySource in direct connection to a population,
		will use this value to calculate the resoruce carrying capacity. This
		requires a value that signalises how much this EnergySource can supply
		totally, which this function provides.
		"""
		return self.energy_out

	# Energy propagation

	def _calc_energy_outflow(self, target: Resource, *, max_flow: float, efficacy: float) -> float:
		"""Calculate the energy outflow from the EnergySource.
		
		Outflow is irrelevant from EnergySource's energy, but depends on the
		fill level of the target resource; receiving less input when coming
		close and passing the energy scale (using logistic curve).
		
		Args:
		    target (Resource): The target resource, usually a secondary res.
		    max_flow (float): The max_flow over the connecting edge
		    efficacy (float): The efficacy of the connecting edge
		
		Returns:
		    float: The energy outflow to the target vertex
		"""
		# Store energy scale to reduce one call to a property map
		energy_scale = target.energy_scale

		# Calculate outflow with sigmoidal function (logistic)
		return (max_flow
		        * (1. - 1.
		                / (1. + math.exp(efficacy
		                                 * (energy_scale - target.energy)
		                                 / energy_scale))))

	def _calc_energy_scaling(self, dE: float) -> None:
		"""The EnergySource can have negative energy, thus no scaling is
		necessary when propagating energy away from it"""
		return None

	def add_roots(self):
		raise NotImplementedError("Not possible for an EnergySource!")


class ConstantFlowEnergySource(EnergySource):
	"""An EnergySource child that always provides a constant flow, regardless
	of its own properties or the target vertex's properties.
	"""

	__slots__ = ('g', 'desc', '_pdict', 'idx', '_linked', 'const_flow')

	def __init__(self, *args, const_flow: float, **kwargs):
		"""Initialise a ConstantFlowEnergySource
		
		Args:
		    *args: Passed to Resource.__init__
		    const_flow (float): The constant flow this energy source provides
				to _each_ vertex. 
		    **kwargs: Passed to Resource.__init__
		"""
		super().__init__(*args, **kwargs)

		self.const_flow = const_flow

	@property
	def available_flow(self) -> float:
		"""If using the EnergySource in direct connection to a population,
		will use this value to calculate the resoruce carrying capacity. This
		requires a value that signalises how much this EnergySource can supply
		totally.

		For the ConstantFlowEnergySource this is the const_flow value.
		"""
		return self.const_flow

	def _calc_energy_outflow(self, *_, **__) -> float:
		"""The outflow from the ConstantFlowEnergySource is always the value
		attributed at initialisation. It does not take into account anything
		else, not even the population size for populations.
		
		Returns:
		    float: The constant flow value.
		"""
		return self.const_flow


class EnergySink(Resource):
	"""The EnergySink collects all energy going out of the network.

	This allows to keep track of the total energy that ever was in the system.
	"""

	__slots__ = ('g', 'desc', '_pdict', 'idx', '_linked')

	propagation_condition = False  # No propagation away from Sink

	def __init__(self, *args, energy=0., **kwargs):
		"""Initialise an EnergySink.

		This is always initialised with reservoir energy zero.
		"""

		super().__init__(*args, energy=0., **kwargs)

		self.type 			= 'energy_sink'
		self.is_not_aux 	= False
		self.is_not_pop 	= True
		self.size 			= -2 # i.e.: not applicable
		# NOTE  The -2 is to distinguish from EnergySource with -1 and make
		# automatic graph plotting work with this value as distinction

	def add_roots(self):
		"""Disallow usage of the add_roots method."""
		raise NotImplementedError("A sink can't have roots!")

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class PrimaryResource(Resource):
	"""A PrimaryResource has an initial energy, but cannot be refilled."""

	__slots__ = ('g', 'desc', '_pdict', 'idx', '_linked')

	def __init__(self, *args, **kwargs):
		"""Initialise a primary resource, i.e. a resource that is not
		replenished over time but where energy can only be _extracted_ from.
		"""

		# Ensure energy_scale being set
		if 'energy_scale' not in kwargs:
			kwargs['energy_scale'] = kwargs.get('energy')

		super().__init__(*args, **kwargs)

		# Set attributes
		self.type 			= 'primary_res'
		self.is_res 		= True
		self.is_res1 		= True
		self.is_not_pop		= True
		self.size 			= -1  # i.e.: not applicable

	def add_roots(self):
		"""Disallow addition of roots."""
		raise NotImplementedError("A primary resource can't have roots!")

class SecondaryResource(Resource):
	"""A SecondaryResource draws energy from the EnergySource and (optionally)
	from other primary resources it is connected to.
	"""

	__slots__ = ('g', 'desc', '_pdict', 'idx', '_linked')

	def __init__(self, *args, energy: float, energy_input: float, scale_factor: float=1., **kwargs):
		"""Initialise a secondary resource.
		
		Args:
		    *args: Passed to Resource.__init__
		    energy (float): The initial energy of the secondary resource
		    energy_input (float): The (maximum) input it receives from the
		    	EnergySource
		    scale_factor (float, optional): The factor that determines the
		    	energy_scale of the resource in multiples of the input. The
		    	unit of the scale factor is thus something like [time]
		    **kwargs: Passed to Resource.__init__
		
		Raises:
		    ValueError: For non-positive scale_factor
		"""

		# Calculate the energy scale
		if scale_factor <= 0.:
			raise ValueError("Need positive `scale_factor`, "
			                 "got "+str(scale_factor))

		energy_scale = energy_input * scale_factor

		# Calculate starting values
		if isinstance(energy, str):
			mode = energy

			if energy in ['input', 'energy_input']:
				energy = energy_input
			elif energy in ['scale', 'energy_scale']:
				energy = energy_scale
			else:
				log.warning("Invalid energy value for %s! Needs to be numeric "
				            "or in ['input', 'scale', 'energy_input', "
				            "'energy_scale'], was '%s'. Setting to zero.",
				            self.__class__.__name__, energy)
				energy = 0.

			log.debugv("Set %s's initial energy to %.3f (mode: %s)",
			           self.__class__.__name__, energy, mode)


		# Initialise Resource using parent method
		super().__init__(*args, energy=energy,
		                 energy_scale=energy_scale, **kwargs)

		# Set attributes
		self.type 			= 'secondary_res'
		self.is_res 		= True
		self.is_res2 		= True
		self.is_not_pop		= True
		self.size 			= -1 # i.e.: not applicable

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class Population(Resource):
	"""Populations are the most complicated parts of the ResourceNetwork.

	They exploit resources (realised indirectly by propagating away from
	resources) and have internal population dynamics which impacts the size of
	the population depending on how much energy they can exploit and how much
	they have in their storage.

	They can accumulate innovations and they can split into two once they reach
	a certain size.
	"""

	__slots__ = ('g', 'idx', '_pdict', 'desc', '_linked',
	             'energy_need', 'birth_rate', 'probabilistic', '_prob_func',
	             '_theta_K', '_K_res_ratio', '_K_eff_ratio',
	             'use_reserves',
	             'split_size', 'split_kwargs', 'ancestors',
	             'p_innov', 'innov_kwargs',
	             'propagation_condition')

	# Managed attributes
	_properties = ('energy', 'size', 'K', 'K_res', 'K_eff', 'theta_K')

	# Class variables -- shared by all instances
	mutation_groups = { 	# which attributes to mutate together
		'birth_death': 		['birth_rate'],
		'resource_expl': 	['use_reserves'],
		'p_innov': 			['p_innov'],
		'split_size': 		['split_size'],
		'split_factor':     [('split_kwargs', 'split_factor')],
		'carrying_cap': 	['theta_K'], # for potentially bundling parameters
		'theta_K': 			['theta_K'],
		'topology': 		[],
		# 'old_innovs':	 	['max_flow', 'efficacy'],
		# 'new_innovs':	 	['max_roots', 'max_flow',
		# 					 'efficacy', 'innov_factor', 'strategy'],
		# Mutation of mutation parameters
		# Grouped
		'mutation_params':  [
			('split_kwargs', 'mutation_params', 'birth_rate', 'sigma'),
			('split_kwargs', 'mutation_params', 'p_innov', 'sigma'),
			('split_kwargs', 'mutation_params', 'theta_K', 'sigma'),
			('split_kwargs', 'mutation_params', 'split_kwargs', 'split_factor', 'sigma')
			],
		'topology_mutation_params':  [
			('split_kwargs', 'mutation_params', 'topology', 'p_loss'),
			('split_kwargs', 'mutation_params', 'topology', 'p_change'),
			('split_kwargs', 'mutation_params', 'topology', 'p_new')
			],
		# All separately
		'mutation_sigma_birth_rate':
			[('split_kwargs', 'mutation_params', 'birth_rate', 'sigma')],
		'mutation_sigma_p_innov':
			[('split_kwargs', 'mutation_params', 'p_innov', 'sigma')],
		'mutation_sigma_theta_K':
			[('split_kwargs', 'mutation_params', 'theta_K', 'sigma')],
		'mutation_sigma_split_factor':
			[('split_kwargs', 'mutation_params', 'split_kwargs', 'split_factor', 'sigma')],
		'topology_mutation_p_loss':
			[('split_kwargs', 'mutation_params', 'topology', 'p_loss')],
		'topology_mutation_p_change':
			[('split_kwargs', 'mutation_params', 'topology', 'p_change')],
		'topology_mutation_p_new':
			[('split_kwargs', 'mutation_params', 'topology', 'p_new')],
	}
	mutation_RNGs = { 		# the RNGs to compute the new value with
		# TODO move hard-coded values to config file
		# keys relate to the name of the property
		'birth_rate': {
			'rng': 			RNG(dist='normal', min_val=0., max_val=0.3),
			'pass_val_as': 	'mu',
			},
		'p_innov': {
			'rng': 			RNG(dist='normal', min_val=0., max_val=1.),
			'pass_val_as': 	'mu',
			},
		'use_reserves': {
			'rng': 			RNG(dist='normal', min_val=0., max_val=1.),
			'pass_val_as': 	'mu',
			},
		('split_kwargs', 'split_factor'): {
			'rng': 			RNG(dist='normal', min_val=1./3., max_val=2./3.),
			'pass_val_as': 	'mu',
			},
		'split_size': {
			'rng': 			RNG(dist='poisson', min_val=2),
			'pass_val_as': 	'lam',
			},
		'theta_K': {
			'rng': 			RNG(dist='normal', mod=2.),
			'pass_val_as': 	'mu',
			},
		# Mutation of mutation parameters
		# NOTE sigmas hard-coded here! Chosen factor 10 smaller than default.
		# TODO check these values are reasonable
		('split_kwargs', 'mutation_params', 'birth_rate', 'sigma'): {
			'rng': 			RNG(dist='normal', min_val=0., max_val=0.1),
			'pass_val_as': 	'mu',
			},
		('split_kwargs', 'mutation_params', 'p_innov', 'sigma'): {
			'rng': 			RNG(dist='normal', min_val=0., max_val=0.1),
			'pass_val_as': 	'mu',
			},
		('split_kwargs', 'mutation_params', 'theta_K', 'sigma'): {
			'rng': 			RNG(dist='normal', min_val=0., max_val=0.1),
			'pass_val_as': 	'mu',
			},
		('split_kwargs', 'mutation_params', 'split_kwargs', 'split_factor', 'sigma'): {
			'rng': 			RNG(dist='normal', min_val=0., max_val=0.1),
			'pass_val_as': 	'mu',
			},
		('split_kwargs', 'mutation_params', 'topology', 'p_loss'): {
			'rng': 			RNG(dist='normal', min_val=0., max_val=1.),
			'pass_val_as': 	'mu',
			},
		('split_kwargs', 'mutation_params', 'topology', 'p_change'): {
			'rng': 			RNG(dist='normal', min_val=0., max_val=1.),
			'pass_val_as': 	'mu',
			},
		('split_kwargs', 'mutation_params', 'topology', 'p_new'): {
			'rng': 			RNG(dist='normal', min_val=0., max_val=1.),
			'pass_val_as': 	'mu',
			}
	}


	def __init__(self, *args, energy, size: int, energy_need: float=0., birth_rate: float=0., probabilistic: Union[bool,str]='gaussian', prob_sigma: float=0.5, theta_K: bool=True, use_reserves: float=0., p_innov: float=0., innov_kwargs: dict=None, split_size: int=0, split_kwargs: dict=None, **kwargs):
		"""Initialise a population.
		
		Args:
		    *args: Passed to Resource.__init__
		    energy (float): energy stored in the population
		    size (int): initial population size
		    energy_need (float, optional): energy needed (per individual)
		    birth_rate (float, optional): the birth rate of the population.
		    	This parameter is used in the population dynamics, which use a
		    	logistic growth formula to calculate the change in population
		    	size.
		    probabilistic (Union[bool, str], optional): Whether population
		    	size change is probabilistic or not. For True or 'poisson', the
		    	Poisson distribution is used. For 'gaussian', a value from a
		    	normal distribution (with prob_sigma) is drawn.
		    prob_sigma (float, optional): The standard deviation of the normal
		    	distribution used for the gaussian probabilistic update.
		    theta_K (bool, optional): The carrying capacity mixing angle
		    use_reserves (float, optional): The fraction with which the reserve
		    	should be incorporated into the calculation of the carrying
		    	capacity.
		    p_innov (float, optional): The innovation probability per round
		    innov_kwargs (dict, optional): The parameters that determine the
		    	effect a single innovation will have
		    split_size (int, optional): at which number the population will
		    	split into two
		    split_kwargs (dict, optional): Further parameters
		    **kwargs: Passed to Resource.__init__
		
		Raises:
		    ValueError: Upon invalid `probabilistic` argument
		
		"""

		# Set private attributes that need initialisation
		self._theta_K 		= None
		self._K_res_ratio	= None
		self._K_eff_ratio	= None

		# Set attributes that are stored in the classes slots
		self.energy_need 	= energy_need
		self.birth_rate 	= birth_rate
		self.theta_K 		= theta_K
		self.use_reserves 	= use_reserves
		self.ancestors 		= []

		self.propagation_condition = True

		if probabilistic is False:
			self.probabilistic = False
			self._prob_func = np.round

		elif probabilistic is True or probabilistic == 'poisson':
			self.probabilistic = 'poisson'
			self._prob_func = lambda lam: (np.random.poisson(lam)
			                               if lam >= 0.
			                               else -np.random.poisson(-lam))
			# TODO consider using the slightly faster random.expovariate

		elif probabilistic == 'gaussian':
			self.probabilistic = 'gaussian'
			self._prob_func = lambda mu: np.round(np.random.normal(mu,
			                                                       prob_sigma))
			# note: important to round here, else it is rounded during application of the size change and thus introduces a drift due to the flooring of the integer casting
			# TODO consider using the slightly faster random.gauss

		else:
			raise ValueError("Invalid argument `probabilistic`! Got '{}', "
			                 "expected 'gaussian' or 'poisson'."
			                 "".format(probabilistic))

		self.p_innov 		= p_innov
		self.innov_kwargs 	= innov_kwargs if innov_kwargs else {}

		self.split_size		= split_size
		self.split_kwargs	= split_kwargs
		self.ancestors 		= [] # stores heredity information as a list of parent indices

		# Convert the dictionaries into DotDicts, i.e. with attribute access
		self.innov_kwargs 	= DotDict(self.innov_kwargs)
		self.split_kwargs 	= DotDict(self.split_kwargs)

		# Calculate a reasonable starting value for the population
		if isinstance(energy, str) and energy == 'auto':
			energy = energy_need * size
			log.debugv("Automatically set Population's initial energy to %.3f", energy)

		# Set up parent
		super().__init__(*args, energy=energy, **kwargs)

		# Set attributes that are stored in the network's propety maps
		self.size 			= size
		self.type 			= 'population'
		self.is_res 		= False
		self.is_res1 		= False
		self.is_res2 		= False
		self.is_pop 		= True
		self.alive	 		= True
		self.num_roots 		= self.in_degree()
		self.num_innovs 	= 0

	@property
	def size(self):
		"""The size of the population. This value is always >= 0."""
		return self['size']

	@size.setter
	def size(self, val):
		if val < 0:
			log.debugv("  Tried to set size of Population vertex %s to a "
			           "negative value: %s. Setting to zero.", self.idx, val)
			val = 0

		val = int(val)
		self['size'] = val

		if val == 0:
			log.debugv("  Population vtx %s: size zero. Deceasing ...",
			           self.idx)
			self.decease()

	@property
	def theta_K(self):
		return self._theta_K

	@theta_K.setter
	def theta_K(self, val):
		"""Sets the theta_K attribute value.

		Additionally calculates the _K_res_ratio and _K_eff_ratio, to reduce
		recalculations.
		"""
		if val != self._theta_K:
			self._theta_K = val
			self._K_res_ratio = math.cos(self.theta_K*math.pi/2.)**2
			self._K_eff_ratio = math.sin(self.theta_K*math.pi/2.)**2

	@property
	def K(self):
		"""Calculates the carrying capacity for this population, i.e. how many individuals can be sustained when looking at the current energy input and a part of the energy reserves.

		The carrying capacity is comprised of the two carrying capacities available to the population: K_res and K_eff, mixed together with the mixing angle theta_K:

		K(theta_K) = sin^2(π/2*theta_K) K_eff + cos^2(π/2*theta_K) K_res

		Thus, for theta_K == 0, the resource carrying capacity is used, for K == 1, the effective carrying capacity is used, and in between a mixed configuration... This allows continuous mutations.

		NOTE: this calculation depends on whether energy was already propagated in this round or not.
		After propagation from the resources to the populations it is:
			self.energy == self.energy_in + energy_reserves_from_last_round
		"""

		# Depending on the mixing angle, calculate the final carrying capacity
		if self.theta_K == 0:
			K = self.K_res

		elif self.theta_K == 1:
			K = self.K_eff

		else:
			# Calculate the mixing with the given ratios. The ratios are saved every time theta_K changes
			K = self.K_res * self._K_res_ratio + self.K_eff * self._K_eff_ratio


		# Calculate the additional carrying capacity due to reserve usage
		K += (self.energy - self.energy_in) * self.use_reserves # >= 0
		# NOTE energy - energy_in is the amount of energy in the reserve; this has to be done this way because at the calculation time of K there was already energy input to the resource which was not yet used up.

		if K > 0.:
			return K

		log.caution("Zero or negative K calculated:  %f. Returning 0.", K)
		return 0.

	@property
	def K_eff(self):
		"""Calculates the *effective* carrying capacity for this population, i.e. how many individuals can be sustained when looking at the current energy input and a part of the energy reserves.
		In contrast, the resource carrying capacity depends on the energy input of all connected resources.

		NOTE: this calculation depends on whether energy was already propagated in this round or not.
		After propagation from the resources to the populations it is:
			self.energy == self.energy_in + energy_reserves_from_last_round
		"""
		return self.energy_in / self.energy_need

	@property
	def K_res(self):
		"""Calculates the resource carrying capacity of this population, i.e. the sum of all energy inputs to all connected resources."""

		E_avail	= 0. # used to gather the potentially available energy to the population from all the connected resources

		# Loop over all connected resources
		for res_vtx in self.in_neighbors():
			res = self.get_vobj(res_vtx)

			# Distinguish between EnergySource, primary resources, and secondary resources
			if res.is_res2:
				# This corresponds to the refill rate of the resource, depending on its fill level
				E_avail += res.energy_in

			elif res.is_res1:
				# Is a primary resource

				# Two options:
				# 1) use the total energy output of the resource. However, this includes the output to all other populations as well (!), which means that the population is able to observe how much of the resource is extracted.
				E_avail += res.energy_out

				# 2) primary resources are not replenished. Thus, it could be argued that they should not contribute directly to the carrying capacity, but (if at all) indirectly via the populations energy reservoir
				# pass

			else:
				# Must be an EnergySource
				E_avail += res.available_flow

		return E_avail / self.energy_need

	# Inherited . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def _calc_energy_outflow(self, target: EnergySink, *, max_flow: float, efficacy: float) -> float:
		"""Overwriting the method set in the parent class, this method
		implements energy propagation away from a Population, i.e.: it models
		the energy being consumed by the population and turned into heat.
		
		Args:
		    target (EnergySink): the target object; this should always be the
		    	energy sink of the network.
		    max_flow (float): The maximum flow rate _per individual_, which
		    	corresponds to the energy need per round.
		    efficacy (float): The efficacy of the connection to the energy sink
		    	does not play a role here, but is passed to the method anyway.
		
		Returns:
		    float: The energy expended by this population at this time step.
		
		Raises:
		    NotImplementedError: energy outflow to types other than the energy
		    	sink.
		"""

		if target.type == 'energy_sink':
			# Calculate energy consumed by the currently living individuals
			E_out = max_flow * self.size

			if E_out <= self.energy:
				# everything ok -- just using up the energy that is available
				pass

			else:
				# The population size requires more energy than available.
				# Need to let individuals die so that not more energy than available is spent.
				try:
					crude_K = self.energy/self.energy_need

				except ValueError as err:
					log.error("Could not calculate crude_K for Population %s, "
					          "setting it to zero. ValueError: %s",
					          self.idx, err)
					crude_K = 0
					new_size = 0

				else:
					if self.probabilistic:
						# Apply the probability function to the difference in population size to have a similar behaviour as during population dynamics.
						new_size = (self.size
						            - self._prob_func(self.size - crude_K))
						# NOTE This way, the new population size can still be above the level of energy available, but this means (storyline-wise) that they learned to survive with less energy ...
					else:
						new_size = crude_K

				if DEBUG:
					log.debugv("   Needing more energy than currently "
					           "available. Can supply %d individuals. "
					           "Dropping population size from %d to %d.",
					           crude_K, self.size, new_size)

				# Correct the carrying capacity fraction
				self.K_frac *= new_size/self.size

				# Set population size
				self.size = new_size

				# Calculate E_out again
				E_out = max_flow * self.size

				# But make sure it is not higher than the available energy
				# TODO check if this is necessary!
				if E_out > self.energy:
					E_out = self.energy

			return E_out

		raise NotImplementedError("Unsupported target type; needs to be "
		                          "EnergySink for now, was '{}' ..."
		                          "".format(type(target.type)))

	def add_roots(self, *args, **kwargs) -> list:
		"""Same as parent, but updates num_roots property afterwards."""
		roots = super().add_roots(*args, **kwargs)

		self.num_roots = self.in_degree()

		return roots

	# Population specific  . . . . . . . . . . . . . . . . . . . . . . . . . .

	def population_dynamics(self):
		"""This method runs the population dynamics of the population and
		calculates how many individuals are to be created.

		Note that the population dynamics are called only after the energy has
		already been propagated from the resources to the population.
		"""

		if self.size <= 0 or not self.birth_rate > 0.:
			log.debugv("Skipping population dynamics of population %d.",
			           self.idx)
			return

		# Save the current carrying capacity such that it needs only be calculated once
		K = self.K

		if DEBUG:
			log.debugv("Performing population dynamics on %s vertex %d ..."
			           "\n\t\t\t  "
			           "E. input:  %-10.2f    E. reserve:  %-10.2f"
			           "\n\t\t\t  " \
			           "K:         %-10.2f    Size:        %d",
			           self.__class__.__name__, self.idx,
			           self.energy_in, self.energy-self.energy_in,
			           K, self.size)

		# Calculate population size change and add to previously left change in pop size
		try:
			if K <= 0.:
				raise ZeroDivisionError
				# NOTE have to do it manually, because self.size returns a numpy.int32 which has no issues with division by zero and would just return np.inf

			dN = self.birth_rate * self.size * (1 - self.size/K)

		except ZeroDivisionError:
			# If the carrying capacity is exactly zero -- which can only be the case if no resources are connected any longer -- a different behaviour has to take place ...
			log.caution("Zero Carrying Capacity! Probabilistically continuing "
			            "population size ...")
			self.size = self._prob_func(self.size)
			self.K_frac = np.inf
			return

		dN_prob = self._prob_func(dN)

		# Apply population size change.
		self.size += dN_prob

		if DEBUG:
			log.debugv("   dN: %d,  after prob_func: %d,  new: %d",
			           dN, dN_prob, self.size)

		# Save carrying capacity fraction (used in monitoring)
		self.K_frac = self.size / K

	def add_innovation(self, p_innov: float=None):
		"""Check if an innovation should be added to this population.
		
		This is determined by the innovation probability and the parameters
		stored in `self.innov_kwargs`.
		
		If the argument p_innov (probability for a new innovation) is not
		given, the attribute with the same name will be used.
		
		Args:
		    p_innov (float, optional): If not given, will use the attribute.
		
		Returns:
		    None, edge: Either None (if no innovation is added) or the edge
		    	that was affected by the innovation.
		
		Raises:
		    KeyError: If identification of the needed edge failed.
		"""

		if self.size == 0:
			return

		if not p_innov:
			p_innov = self.p_innov

		if random.random() > self.p_innov:
			return

		innov_kwargs = self.innov_kwargs
		max_num_roots = innov_kwargs['max_roots']
		strategy = innov_kwargs['strategy']
		strategy_kwargs	= innov_kwargs.get('strategy_kwargs', {})

		log.debug("Adding innovation with strategy '%s' ...", strategy)

		# Find the method with which to determine the source vertex
		src_finder = partial(self.vtx_finder, strategy=strategy)

		# Get list of current roots to resources
		roots = list(self.in_neighbors())

		if len(roots) < max_num_roots:
			# Determine the source vertex from all available ones
			src_vtx = src_finder(**strategy_kwargs)[0]
		else:
			# Maximum number of roots is reached -> select among those
			src_vtx = src_finder(vtx_list=roots, **strategy_kwargs)[0]

		# Shortcut for edge property maps (CAREFUL: is a PropertyMap!)
		max_flows = self.g.ep['max_flow']
		efficacies = self.g.ep['efficacy']

		# Check if the source vertex is already present
		if len(roots) >= max_num_roots or src_vtx in roots:
			# Is already present -> adjust edge properties

			# Get the corresponding edge
			innov_edge = self.g.edge(src_vtx, self.idx)
			# TODO for higher-order innovations: have to find the edge in a different way ...

			if innov_edge is None:
				raise KeyError("Could not associate an edge descriptor for "
				               "src_vtx "+str(src_vtx))

			log.debug("Improving innovation %s ...", innov_edge)

			# Adjust edge properties
			innov_factor = call_if_rng(innov_kwargs['innov_factor'],
			                          name="innov_factor")

			max_flows[innov_edge] *= innov_factor
			efficacies[innov_edge] *= innov_factor
			if efficacies[innov_edge] > 1.:
				efficacies[innov_edge] = 1.

			log.debug("Innovation (%s -> %s) improved.", *innov_edge)

		else:
			# Not present yet -> add a new edge
			innov_edge = self.g.add_edge(source=src_vtx, target=self.desc)
			# TODO add innovation objects here as well

			# Set edge properties
			max_flows[innov_edge] = call_if_rng(innov_kwargs['max_flow'],
			                                    name="max_flow")
			efficacies[innov_edge] = call_if_rng(innov_kwargs['efficacy'],
			                                     name="efficacy")
			if efficacies[innov_edge] > 1.:
				efficacies[innov_edge] = 1.

			# Update number of roots and leaves
			self.num_roots = self.in_degree()
			self.g.vp['num_leaves'].a[int(src_vtx)]	+= 1

			log.debug("Innovation (%s -> %s) added.", src_vtx, self.desc)

		# Innovation added. Increment number of innovations
		self.num_innovs += 1

		# Done
		return innov_edge

	def receive_mutations(self, only: list=None) -> None:
		"""This method can be called when a population is in the position 
		where external events lead to a mutation of internal parameters.
		
		Which mutations take place depend on the mutation rates; the
		parameters that possibly change depend on the mutation groups the
		parameters are associated with.
		
		Args:
		    only (list, optional): sequence of the names of the mutations to
		    be performed. If not given, the list set in self.split_kwargs is
		    used.
		
		Returns:
		    None
		
		Raises:
		    NotImplementedError: For an advanced pattern of how the attributes
		    	that are to be mutated are specified.
		"""

		def parse_params(val=None, pass_val_as: str=None, **kws) -> dict:
			"""Prepares the parameters to call the RNG with ..."""
			d = dict(**kws)
			d[pass_val_as] = val  # parameter name as key, value as value
			return d

		# Get default value, if no list was given
		mutations = only if only else self.split_kwargs.get('mutations', [])

		if not mutations:
			return

		# Get the mutation parameters of this population
		params = self.split_kwargs['mutation_params']

		# Loop over the names of the mutation groups
		for group in mutations:

			# Special case for topological changes
			if group == 'topology':
				self.topology_mutations()
				continue

			# For all other -- i.e. internal -- properties, get the names of the relevant parameters to mutate ...
			props = self.mutation_groups[group]
			log.debugv("Mutating group %s, properties: %s", group, props)

			# Now loop over the properties to be mutated ...
			for prop in props:
				# Distingish between tuple and string properties
				if isinstance(prop, tuple):
					# Got a sequence of keys
					# Get the reference object, starting from the population
					ref_obj = self

					# Walk along the keys
					for key in prop[:-1]:
						# Try to get the reference object
						# First via attribute, then item
						try:
							# Via attribute
							ref_obj = getattr(ref_obj, key)

						except AttributeError:
							# Try again via item access
							try:
								ref_obj = ref_obj[key]

							except (KeyError, IndexError) as err:
								raise KeyError("No attribute or item '{}' "
							    	           "available for object {}!"
							        	       "".format(key,
							        	                 ref_obj)) from err

					# Get the mutation parameters
					mutation_params = params[".".join(prop)]

					# Property name is last key
					prop_name = prop[-1]

				else:
					# The reference object is just the population itself
					ref_obj = self

					# The property name is just the property
					prop_name = prop

					# Get old value
					old_val = getattr(ref_obj, prop)

					# Get the mutation parameters, available on top level
					mutation_params = params[prop] 

				# Set up rng and create new value
				rng = self.mutation_RNGs[prop]['rng']
				pass_as	= self.mutation_RNGs[prop]['pass_val_as']
				kwargs = parse_params(val=old_val, pass_val_as=pass_as,
				                      **mutation_params)
				new_val = rng(**kwargs)

				log.debugv("   %s:   %s -> %s", prop_name, old_val, new_val)

				# Set the new value
				setattr(ref_obj, prop_name, new_val)

		# NOTE need not set the flag for updating the genealogy here; the vertex there is only created after the mutations have been performed; thus, the mutations are taken into account.

		return

	def topology_mutations(self, probs: dict=None):
		"""Carries out topologicalal mutations.
		
		There are three kinds of mutations:
		- loss: lose an already exiting innovation completely
		- change: change an innovation (NotImplemented!)
		- new: gain a new innovation
		
		Is usually called from the receive_mutations method, but can also be
		called on its own.

		Args:
		    probs (dict, optional): The probabilities for each type of
		    	mutation to occur. If not given, will use the ones from the
		    	mutation parameters.
		    	Expected keys are 'p_loss', 'p_change', and 'p_new'. Note that
		    	'p_loss' is the probability _per already exiting edge_.
		
		Raises:
		    NotImplementedError: For p_change > 0
		"""
		log.debug("Carrying out topological mutations on Population %s ...",
		          self.idx)

		if not probs:
			probs = self.split_kwargs['mutation_params']['topology']

		# Lose innovations (probability _per existing innovation_)
		if probs.get('p_loss', 0) > 0:
			p_loss = probs['p_loss']
			log.debug("Innovation loss enabled with probability %s", p_loss)

			lose = [e for e in self.in_edges() if random.random() < p_loss]
			log.debugv("Edges that will get lost due to mutation:\n%s", lose)

			for res_vtx, _ in lose:
				log.debugv("Deleting edge %s -> %s ...", res_vtx, self.idx)

				# Remove the edge using the edge descriptor
				self.g.remove_edge(self.g.edge(res_vtx, self.idx))

			log.debugv("Finished deletions.")

		# Gain a new innovation
		if probs.get('p_new', 0) > 0:
			p_new = probs['p_new']
			log.debug("New innovation with probability %s", p_new)

			innov_edge = self.add_innovation(p_innov=p_new)
			if innov_edge:
				log.debugv("Added a new innovation: %s", innov_edge)
			else:
				log.debugv("No new innovation added.")

		if probs.get('p_change', 0) > 0:
			raise NotImplementedError("p_change")
			# p_change = probs['p_change']
			# log.debug("Changing innovation with probability %s", p_change)

	def decease(self):
		"""Can be called once a population reaches size 0.

		Takes care of setting some properties accordingly.
		"""

		log.debugv("Population %s deceased. Setting PropertyMaps accordingly, "
		           "deactivating edges, setting flags, ...", self.desc)

		# Set class and slot values
		# ...

		# Set PropertyMap values of the Resource network
		self.alive = False
		self.demise = self.g.location.steps
		self.K_frac = np.nan

		# ...and more of the genealogy
		self.set_link_item('alive', False)
		self.set_link_item('demise', self.g.location.steps)

		# Deactivate propagation -- this only disables propagation *away* from this population
		self.propagation_condition = False

		# Deactivate edges and adjust source and target degree
		for edge in self.desc.all_edges():
			self.g.ep['inactive'][edge] = True

			s, t = edge
			self.g.vp['num_roots'].a[int(t)] -= 1
			self.g.vp['num_leaves'].a[int(s)] -= 1

		# Propagate away the remaining energy
		dE = self.energy
		self.energy 		= 0.
		self.energy_out 	+= dE
		self.delta_energy 	-= dE

		sink = self.get_vobj(self.g.sink_vtx)
		sink.energy 		+= dE
		sink.energy_in 		+= dE
		sink.delta_energy 	+= dE

		# Done.

	# Genealogy . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def _init_new_linked_vtcs(self, only_new_link: bool=False):
		"""In the case of the population, the only linked vertex is that in
		the genealogy. This method is called when the Population object is
		created and got a link to the genealogy network passed.

		When this is called, a vertex was already created in the genealogy.
		What's left to set up are the relevant attributes and connections.
		"""
		if len(self._linked) > 1:
			raise ValueError("Unexpected number of linked networks for "
			                 "Population. Can only handle one. Currently set "
			                 "links are:" + str(self._linked))

		tree = self._linked[0]['g']

		if self.ancestors:
			log.debugv("  Is a child node. Ancestors: %s", self.ancestors)

			# Add an edge from parent to child
			child_idx = self._linked[0]['desc']
			parent_idx = self.ancestors[-1][1] # the index of the parent

			e = tree.add_edge(source=parent_idx, target=child_idx)
			log.debugv("  Added edge:  %s", tuple(e))

		else:
			log.debugv("  Is a root node.")

		# Save information to the genealogy.
		self.set_link_item('pop_index', self.idx)
		self.set_link_item('generation', len(self.ancestors))
		if self.g.location:
			self.set_link_item('birth', self.g.location.steps)

		for attr_name in tree.INIT_FROM_RNET:
			if isinstance(attr_name, tuple):
				src_attr, target_attr = attr_name
			else:
				src_attr, target_attr = attr_name, attr_name

			log.debugv("  Setting attribute %s ...", target_attr)
			self.set_link_item(target_attr, getattr(self, src_attr))

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# TODO add Innovation object for edges

# class Innovation(EdgeObject):

# 	# __slots__	= () # additional slots (can be empty, but needs to be defined)
# 	__slots__ = ('g', 'desc', '_pdict')

# 	def __init__(self, *nwo_args, max_flow: float, efficacy: float=1., **nwo_kwargs):
# 		"""Initialise an Innvation object as a VertexObject with mode 'edge'

# 		Args:
# 			*nwo_args, **nwo_kwargs : passed on to parent VertexObject

# 		"""
# 		super().__init__(*nwo_args, **nwo_kwargs)

# 		# Set attributes
# 		self.max_flow = max_flow
# 		self.efficacy = efficacy

