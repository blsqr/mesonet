#!/usr/bin/env python3

from setuptools import setup

# Dependency lists
install_deps = [
    'numpy>=1.14',
    'scipy>=1.0',
    # NOTE the below deps need to have been installed prior to setup
    'deeevolab>=0.6',
    ]
test_deps = ['pytest>=3.4.0', 'pytest-cov>=2.5.1']

# -----------------------------------------------------------------------------
setup(name='mesonet',
      version='0.4',  # NOTE also adjust this in mesonet/__init__.py
      description="An evolutionary interaction network of populations and resources",
      # long_description=(""),
      author="Yunus Sevinchan",
      author_email='Yunus.Sevinchan@iup.uni.heidelberg.de',
      url='https://ts-gitlab.iup.uni-heidelberg.de/yunus/mesonet',
      classifiers=[
          'Intended Audience :: Science/Research',
          'Programming Language :: Python :: 3.6',
      ],
      packages=['mesonet'],
      package_data=dict(mesonet=["*.yml", "**/*.yml"]),
      # scripts=['cli/dvl-run', 'cli/dvl-eval'],
      install_requires=install_deps,
      tests_require=test_deps,
      test_suite='py.test',
      extras_require=dict(test_deps=test_deps)
)
