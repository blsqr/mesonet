"""This module tests the Genealogy class."""

import os
import pkg_resources

import numpy as np
import pytest

import deeevolab as dvl
dvl.set_verbosity(4)
from deeevolab.multiverse import Multiverse

from mesonet import Mesonet
import mesonet.rnet_objs as rno

# Local variables
TEST_CFG = pkg_resources.resource_filename('tests', 'cfg/test_cfg.yml')

# Fixtures --------------------------------------------------------------------
