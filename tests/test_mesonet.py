"""This module tests the Mesonet universe."""

import os
import pkg_resources

import numpy as np
import pytest

import deeevolab as dvl
dvl.set_verbosity(4)
from deeevolab.multiverse import Multiverse

from mesonet import Mesonet
import mesonet.rnet_objs as rno

# Local variables
TEST_CFG = pkg_resources.resource_filename('tests', 'cfg/test_cfg.yml')

# Fixtures --------------------------------------------------------------------

@pytest.fixture
def cfg() -> dict:
	"""Load a configuration for the Mesonet Universe"""
	return dvl.load_cfg_file(TEST_CFG)

@pytest.fixture
def uni_cfg(tmpdir, cfg) -> dict:
	"""Return a base configuration that can be used to initialise Mesonet directly"""
	cfg = cfg['pspace'].default
	return dict(base_dir=tmpdir, max_num_steps=23, **cfg['init_kwargs'])

@pytest.fixture
def mv(tmpdir, cfg) -> Multiverse:
	"""Returns a Multiverse of the Mesonet universe"""
	return Multiverse(Mesonet, out_basedir=tmpdir, **cfg)

@pytest.fixture
def plain_uni(uni_cfg) -> Mesonet:
	"""Runs a short universe simulation, then returns the universe"""
	uni_cfg['actions'] = {}
	uni_cfg['max_num_steps'] = 10 # including initial step

	# Initialise the universe and run a short simulation
	uni = Mesonet(**uni_cfg)
	uni.run_simulation(num_steps=2)

	return uni

# Tests -----------------------------------------------------------------------

def test_init(uni_cfg):
	"""Test initialisation"""
	Mesonet(**uni_cfg)

def test_run(plain_uni):
	"""Test a simple run"""
	uni = plain_uni

	# Let one population decease
	pops = [uni.network.vobjs[v] for v in uni.network.gvs['pops'].vertices()]
	pops[0].decease()

	# Continue running
	uni.run_simulation(num_steps=2)

def test_via_multiverse(tmpdir, cfg):
	"""Test the initialisation of a Multiverse object. Mainly coverage test."""
	rno.FLOW_ENABLED = True
	mv = Multiverse(Mesonet, out_basedir=tmpdir, **cfg)
	uni = mv.run_single()

def test_action_add_vertices(tmpdir, uni_cfg):
	"""Tests add_vertices action
	"""
	# Initialise the universe
	uni = Mesonet(**uni_cfg)
	
	# Number of vertices before application of the action
	num_vtx_before = uni.network.num_vertices()

	# Apply the action
	res = uni._actn_add_vertices(vtx_type='res1', num=2, energy=123.45)

	# Check that they were added correctly
	assert uni.network.num_vertices() == num_vtx_before + 2
	assert len(res) == 2
	assert [r for r in res.values()][0]['energy'] == 123.45
	assert [r for r in res.values()][0]['type'] == 'res1'

	# Assert that it does not work with an invalid resource type
	with pytest.raises(ValueError, match="Allowed vertex types: "):
		uni._actn_add_vertices(vtx_type='not a valid vertex type')

def test_sc_const_pop_size(plain_uni):
	"""Test the const_pop_size stop condition"""
	uni = plain_uni
	mon = uni.monitors['population_sizes']
	
	# coverage tests
	uni._sc_const_pop_size(const_steps=1, tol_val=np.inf, mode=None)
	uni._sc_const_pop_size(const_steps=1, tol_val=np.inf, mode='rel_std')
	uni._sc_const_pop_size(const_steps=1, tol_val=np.inf, mode='abs_std')
	uni._sc_const_pop_size(const_steps=1, tol_val=np.inf, mode='acc')
	with pytest.raises(ValueError):
		uni._sc_const_pop_size(const_steps=1, tol_val=0.05, mode='foo')

	# functionality
	# mode: None
	assert not uni._sc_const_pop_size(const_steps=2, mode=None)
	uni.monitors['population_sizes'] = np.array(mon.records)
	uni.monitors['population_sizes'][uni.steps-2:,:] = 42
	assert uni._sc_const_pop_size(const_steps=2, mode=None)

def test_sc_extinction(plain_uni):
	"""Test the extinction stop condition"""
	uni = plain_uni
	mon = uni.monitors['population_sizes']

	# by default, they should not be extinct
	assert not uni._sc_extinction()
	
	# with sizes zero, they should be
	uni.monitors['population_sizes'] = np.array(mon.records)
	uni.monitors['population_sizes'][uni.steps-1,:] = 0
	assert uni._sc_extinction()
	
def test_sc_domination(plain_uni):
	"""Test the domination stop condition"""
	uni = plain_uni
	mon = uni.monitors['population_sizes']

	# by default, there should be no domination
	assert not uni._sc_domination(look_back=10)  # not that many steps
	assert not uni._sc_domination()  # two pops should be alive

	# let only the first population be alive -> domination
	uni.monitors['population_sizes'] = np.array(mon.records)
	uni.monitors['population_sizes'][:,1:] = 0
	assert uni._sc_domination()
	
def test_sc_const_K_frac(plain_uni):
	"""Test the const_K_frac stop condition"""
	uni = plain_uni
	mon_name = 'population_carrying_capacity_fraction'
	mon = uni.monitors[mon_name]

	# Always False for large number of constant steps
	assert not uni._sc_const_K_frac(const_steps=np.inf)

	# by default, these should not be constant
	assert not uni._sc_const_K_frac(const_steps=2)
	assert not uni._sc_const_K_frac(const_steps=2, rel_tol=0.00001)

	# With high tolerance, it should be asserted True
	assert uni._sc_const_K_frac(const_steps=2, rel_tol=np.inf)

	# TODO set the actual monitor values
	
def test_sc_const_pop_engy(plain_uni):
	"""Test the const_pop_engy stop condition"""
	uni = plain_uni
	mon_name = 'energy_monitor'
	mon = uni.monitors[mon_name]
	# NOTE use the 'population_energy' column

	# Always False for large number of constant steps
	assert not uni._sc_const_pop_engy(const_steps=np.inf)

	# by default, these should not be constant
	assert not uni._sc_const_pop_engy(const_steps=2)
	assert not uni._sc_const_pop_engy(const_steps=2, rel_tol=0.00001)

	# With high tolerance, it should be asserted True
	assert uni._sc_const_pop_engy(const_steps=2, rel_tol=np.inf)

	# TODO set the actual monitor values
