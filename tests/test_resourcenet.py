"""This module tests the ResourceNetwork."""

import os
import pkg_resources

import numpy as np
import pytest

import deeevolab as dvl
from deeevolab.multiverse import Multiverse

from mesonet import Mesonet, ResourceNetwork
import mesonet.rnet_objs as rno

# Local variables
TEST_CFG = pkg_resources.resource_filename('tests', 'cfg/test_cfg.yml')

# Fixtures --------------------------------------------------------------------

@pytest.fixture
def cfg() -> dict:
	"""Load a configuration for the Mesonet Universe"""
	return dvl.load_cfg_file(TEST_CFG)

@pytest.fixture
def uni_cfg(tmpdir, cfg) -> dict:
	"""Return a base configuration that can be used to initialise Mesonet directly"""
	cfg = cfg['pspace'].default
	return dict(base_dir=tmpdir, max_num_steps=10, **cfg['init_kwargs'])

@pytest.fixture
def mesonet_rnet(uni_cfg) -> Mesonet:
	"""Returns an rnet that was initialised by Mesonet and its test cfg"""
	uni_cfg['actions'] = {}

	# Initialise the universe and run a short simulation
	uni = Mesonet(**uni_cfg)

	return uni.network

@pytest.fixture
def rnet() -> ResourceNetwork:
	"""Initialises a plain and empty ResourceNetwork"""
	return ResourceNetwork()

@pytest.fixture
def res1_kwargs() -> dict:
	"""kwargs to initialise a primary resource"""
	return dict(energy=1)

@pytest.fixture
def res2_kwargs() -> dict:
	"""kwargs to initialise a secondary resource"""
	return dict(energy_input=1, energy=1)

@pytest.fixture
def pop_kwargs() -> dict:
	"""kwargs to initialise a population"""
	return dict(energy=1, size=23)

# Tests -----------------------------------------------------------------------

def test_init(mesonet_rnet):
	"""Initialisation test... also via fixture"""
	rnet = ResourceNetwork()

def test_add_source_and_sink(rnet):
	"""Tests the add_source_and_sink method"""
	# Add the source and sink
	rnet.add_source_and_sink()

	# Trying to add them again should raise
	with pytest.raises(RuntimeError):
		rnet.add_source_and_sink()

	# Also try adding a constant flow source
	# have to remove them first to do that (does not actually remove the vertices!)
	rnet.src_vtx = None
	rnet.sink_vtx = None
	rnet.add_source_and_sink(const_flow=1000.)

def test_add_primary_resource(rnet, res1_kwargs):
	"""Test adding a primary resource to the network"""
	rnet.add_primary_resource(**res1_kwargs)

def test_add_secondary_resource(rnet, res2_kwargs):
	"""Test adding a secondary resource to the network"""
	# Should fail without a source, i.e. no vertex should be added
	with pytest.raises(ValueError):
		rnet.add_secondary_resource(**res2_kwargs)
	assert rnet.num_vertices() == 0

	# Add the source, retry
	rnet.add_source_and_sink()
	rnet.add_secondary_resource(**res2_kwargs)
	assert rnet.num_vertices() == 3
	assert isinstance(rnet.vobjs[2], rno.SecondaryResource)

def test_add_population(rnet, pop_kwargs):
	"""Test adding a population to the network"""
	# Should fail without a sink, i.e. no vertex should be added
	with pytest.raises(ValueError):
		rnet.add_population(**pop_kwargs) is None
	assert rnet.num_vertices() == 0

	# Add the sink
	rnet.add_source_and_sink()
	rnet.add_population(**pop_kwargs)
	assert rnet.num_vertices() == 3
	assert isinstance(rnet.vobjs[2], rno.Population)

	# Connect the new population to the source directly
	pop_vtx, _ = rnet.add_population(src_root=dict(connect=True, max_flow=1.),
	                                 **pop_kwargs)
	assert rnet.edge(rnet.src_vtx, pop_vtx) is not None

def test_pop_splitting(mesonet_rnet):
	"""Test the splitting of populations."""
	rnet = mesonet_rnet
	old_num_vertices = rnet.num_vertices()
	old_num_edges = rnet.num_edges()

	# Get the population objects
	pops = [rnet.vobjs[pvtx] for pvtx in rnet.gvs['pops'].vertices()]

	# Set the split size, perform a split, and check that everything is correct
	pop = pops[0]
	pop.split_size = int(pop.size * 0.8)
	pop.split_kwargs['mutations'] = ['p_innov', 'carrying_cap', 'birth_death',
								     'split_size', 'split_factor']
	rnet.population_splitting()

	assert rnet.num_vertices() == old_num_vertices + 1
	assert rnet.num_edges() == old_num_edges + pop.in_degree() + 1

	# Split again, with topology mutations
	pop = pops[1]
	pop.split_size = int(pop.size * 0.8)
	pop.split_kwargs['mutations'] = ['topology']
	rnet.population_splitting()
