"""This module tests the NetworkObject-derived classes for Mesonet."""

import os
import pkg_resources

import numpy as np
import pytest

import deeevolab as dvl
from deeevolab.multiverse import Multiverse

from mesonet import Mesonet, ResourceNetwork
import mesonet.rnet_objs as rno

# Local variables
TEST_CFG = pkg_resources.resource_filename('tests', 'cfg/test_cfg.yml')

# Fixtures --------------------------------------------------------------------

@pytest.fixture
def cfg() -> dict:
	"""Load a configuration for the Mesonet Universe"""
	return dvl.load_cfg_file(TEST_CFG)

@pytest.fixture
def uni_cfg(tmpdir, cfg) -> dict:
	"""Return a base configuration that can be used to initialise Mesonet directly"""
	cfg = cfg['pspace'].get_default()
	return dict(base_dir=tmpdir, max_num_steps=10, **cfg['init_kwargs'])

@pytest.fixture
def mesonet_rnet(uni_cfg) -> Mesonet:
	"""Returns an rnet that was initialised by Mesonet and its test cfg"""
	uni_cfg['actions'] = {}

	# Initialise the universe and run a short simulation
	uni = Mesonet(**uni_cfg)

	return uni.network

@pytest.fixture
def rnet() -> ResourceNetwork:
	"""Initialises a plain and empty ResourceNetwork"""
	return ResourceNetwork()

@pytest.fixture
def res1_kwargs() -> dict:
	"""kwargs to initialise a primary resource"""
	return dict(energy=1)

@pytest.fixture
def res2_kwargs() -> dict:
	"""kwargs to initialise a secondary resource"""
	return dict(energy_input=1, energy=1)

@pytest.fixture
def pop_kwargs() -> dict:
	"""kwargs to initialise a population"""
	return dict(energy=1, size=23)


# Tests -----------------------------------------------------------------------

def test_Resource(rnet):
	"""Tests the Resource class"""
	# Manually add the vertex
	vtx = rnet.add_vertex()

	# Initialisation
	# Should fail for negative energy
	with pytest.raises(ValueError):
		rno.Resource(g=rnet, desc=vtx, energy=-1.)

	# Settings above ZERO_TOL should not fail
	res = rno.Resource(g=rnet, desc=vtx, energy=rno.ZERO_TOL*0.1)
	assert res.energy == 0.
	
	# Normal case
	res = rno.Resource(g=rnet, desc=vtx, energy=1.)

@pytest.mark.skip("Needs to be written!")
def test_energy_propagation(rnet):
	"""Tests whether energy propagation works as desired"""
	pass

def test_vtx_finder(rnet, pop_kwargs):
	"""Test the vtx_finder function"""
	rnet.add_source_and_sink()
	vtx, pop = rnet.add_population(**pop_kwargs)

	# Check root addition
	with pytest.raises(ValueError):
		pop.add_roots(max_flow=1.)

	# No other vertices yet
	assert pop.vtx_finder(strategy='random', num=1) is None

	# Add some
	for _ in range(10):
		rnet.add_primary_resource(energy=1.)

	# Test different arguments
	assert pop.vtx_finder(strategy='random', num=0) is None
	assert len(pop.vtx_finder(strategy='random')) == 1
	assert len(pop.vtx_finder(strategy='random', num=2)) == 2
	assert len(pop.vtx_finder(strategy='random', num=2, shuffle=False)) == 2
	assert len(pop.vtx_finder(strategy='random', num=10)) == 10
	assert len(pop.vtx_finder(strategy='random', num=12)) == 10
	assert len(pop.vtx_finder(strategy='random', num=-2)) == 8
	assert pop.vtx_finder(strategy='random', num=-10) is None

	# Test different strategies
	vtcs = pop.vtx_finder(strategy='by_index', num=2, vtx_list=[1, 2, 3, 4, 5])
	assert len(vtcs) == 2
	assert vtcs[0] == rnet.vertex(1)
	assert vtcs[1] == rnet.vertex(2)

	vtx, _ = rnet.add_primary_resource(energy=1001.)
	assert pop.vtx_finder(strategy='largest_energy') == [vtx]
	
	assert pop.vtx_finder(strategy='least_out_conns')

	# Need secondary resources and propagation for testing of some strategies
	vtx_low, _ = rnet.add_secondary_resource(energy_input=1.,
	                                         energy=1.)
	vtx_high, _ = rnet.add_secondary_resource(energy_input=10000.,
	                                          energy=10000.)

	# Connect the population to all resources
	pop.add_roots(max_flow=1., strategy='all', select_from='res1')
	pop.add_roots(max_flow=1., strategy='all', select_from='res2')
	# FIXME `select_from='res'` somehow also connects the pop to itself!

	rnet.propagate_energy()

	assert pop.vtx_finder(strategy='lowest_input') == [vtx_low]
	assert pop.vtx_finder(strategy='highest_input') == [vtx_high]
	
	assert pop.vtx_finder(strategy='lowest_output')
	assert pop.vtx_finder(strategy='lowest_output',
	                      select_from='res2') == [vtx_low]
	assert pop.vtx_finder(strategy='highest_output') == [vtx_high]
	assert pop.vtx_finder(strategy='highest_output',
	                      select_from='res2') == [vtx_high]

	# FIXME the below method does not currently work
	# assert pop.vtx_finder(strategy="random_maxdist", max_dist=1)
	# assert pop.vtx_finder(strategy="random_maxdist", max_dist=2) is None
	# NOTE there is no vertex at distance >= 2
